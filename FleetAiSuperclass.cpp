//
//  FleetAiSuperclass.cpp
//
//  DO NOT MODIFY THIS FILE
//

#include <cassert>
#include <vector>

#include "ObjLibrary/Vector3.h"

#include "WorldInterface.h"
#include "PhysicsObjectId.h"
#include "PhysicsObject.h"
#include "ShipAiInterface.h"
#include "Ship.h"
#include "AiShipReference.h"
#include "Marker.h"
#include "FleetAiSuperclass.h"

using namespace std;



void FleetAiSuperclass :: draw () const
{
	assert(isInitialized());

	;  // do nothing
}



void FleetAiSuperclass :: updateForPause ()
{
	; // do nothing
}



FleetAiSuperclass :: FleetAiSuperclass ()
		: m_command_ship(),
		  mv_fighters(FIGHTER_COUNT, AiShipReference()),
		  mvp_markers(MARKER_COUNT,  NULL)
{
	assert(invariant());
}

FleetAiSuperclass :: FleetAiSuperclass (const FleetAiSuperclass& original)
		: m_command_ship(),
		  mv_fighters(FIGHTER_COUNT, AiShipReference()),
		  mvp_markers(MARKER_COUNT,  NULL)
{
	assert(!original.isInitialized());

	assert(invariant());
}

FleetAiSuperclass :: FleetAiSuperclass (const FleetAiSuperclass& original,
                                        AiShipReference command_ship,
                                        vector<AiShipReference>& v_fighters,
                                        vector<Marker*>& vp_markers)
		: m_command_ship(command_ship),
		  mv_fighters(v_fighters),
		  mvp_markers(vp_markers)
{
	assert(original.isInitialized());
	assert(command_ship.isShip());
	assert(v_fighters.size() == FIGHTER_COUNT);
	for(unsigned int i = 0; i < FIGHTER_COUNT; i++)
		assert(v_fighters[i].isShip());
	assert(vp_markers.size() == MARKER_COUNT);
	for(unsigned int i = 0; i < MARKER_COUNT; i++)
		assert(vp_markers[i] != NULL);

	assert(invariant());
}

//
//  Assignment Operator
//
//  This function has intentionally not been implemented.
//
//  FleetAiSuperclass& FleetAiSuperclass :: operator= (const FleetAiSuperclass& original);
//

void FleetAiSuperclass :: setShipsAndMarkers (AiShipReference command_ship,
                                              std::vector<AiShipReference>& v_fighters,
                                              std::vector<Marker*>& vp_markers)
{
	assert(command_ship.isShip());
	assert(v_fighters.size() == FIGHTER_COUNT);
	for(unsigned int i = 0; i < FIGHTER_COUNT; i++)
		assert(v_fighters[i].isShip());
	assert(vp_markers.size() == MARKER_COUNT);
	for(unsigned int i = 0; i < MARKER_COUNT; i++)
		assert(vp_markers[i] != NULL);

	m_command_ship = command_ship;
	mv_fighters    = v_fighters;
	mvp_markers    = vp_markers;

	assert(invariant());
}

bool FleetAiSuperclass :: invariant () const
{
	if(mv_fighters.size() != FIGHTER_COUNT) return false;
	if(mvp_markers.size() != MARKER_COUNT) return false;

	for(unsigned int i = 0; i < FIGHTER_COUNT; i++)
		if(m_command_ship.isShip() != mv_fighters[i].isShip())
			return false;

	for(unsigned int i = 0; i < MARKER_COUNT; i++)
		if(m_command_ship.isShip() != (mvp_markers[i] != NULL))
			return false;

	return true;
}
