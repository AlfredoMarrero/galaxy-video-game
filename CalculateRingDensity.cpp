//
// ALfredo Marrero
// 200 330 199
// interface for class CoordinateSystem
// CalculateRingDensity.cpp


#include "CalculateRingDensity.h"
#include "ObjLibrary/ObjModel.h"


// constructor
CalculateRingDensity::CalculateRingDensity(){


	ringY_halfLenght = 12000;
	ringXZdistance = 150000;
	planetRadius = 60000;

	//moonA
	moons[0].x = 0.0;
	moons[0].z = 140000.0;
	moons[0].radius =3300.0;
	//moonB
	moons[1].radius = 4300.0;
	moons[1].x = 85000.0;
	moons[1].z = 75000.0;
	//moonC
	moons[2].radius = 2000.0;
	moons[2].x = 130000.0;
	moons[2].z = 40000.0;
	//moonD
	moons[3].radius = 3400.0;
	moons[3].x = 110000.0;
	moons[3].z = -60000.0;
	//moonE
	moons[4].radius = 5000.0;
	moons[4].x = 100000.0;
	moons[4].z = -70000.0;
	//moonF
	moons[5].radius = 3100.0;
	moons[5].x = 20000.0;
	moons[5].z = -135000.0;
	//moonG
	moons[6].radius = 2600;
	moons[6].x = -60000;
	moons[6].z = -80000;
	//moonH
	moons[7].radius = 2200.0;
	moons[7].x = -95000.0;
	moons[7].z = -70000.0;
	//moonI
	moons[8].radius = 4700.0;
	moons[8].x = -90000.0;
	moons[8].z = -40000.0;
	//moonJ
	moons[9].radius = 3800.0;
	moons[9].x = -100000.0;
	moons[9].z = 50000.0;

	moons[10].x = 0.0;
	moons[10].z = 0.0;
	moons[10].radius = 75000.0;

}


//The raw density for p with respect to the disk (basic pancake) 
double CalculateRingDensity::rawDensityForP(Vector3 p){

	double XZdistance;
	double r;

	XZdistance = sqrt(p.x*p.x +p.z*p.z);

	if (XZdistance < 150000){
		r = 12000 - abs(p.y);
	}
	else if(XZdistance >= 150000){
		r = 12000 - sqrt( p.y*p.y + (XZdistance - 150000)*(XZdistance - 150000));
	}
	return r;
}


//Calculate the raw density with respect to each hole in the ring
double CalculateRingDensity::rawDensityWithRespectToHole(Vector3 p){

	double rawDensity, tempDensity; 
	Vector3 objectCenter;
	double minRawDensity = rawDensityForP(p);

	for (int i = 0; i < NumberOfMoonsAndP; i++){
		objectCenter.x = moons[i].x;
		objectCenter.y = 0.0;
		objectCenter.z = moons[i].z;

		tempDensity = p.getDistance(objectCenter);

		if(moons[i].radius == 75000){
			rawDensity =tempDensity - 75000;
		}
		else {
			rawDensity = tempDensity - (moons[i].radius + 2000);
		}

		if(rawDensity < minRawDensity){
			minRawDensity = rawDensity;
		}
	}

	return minRawDensity;
}



//Calculate the curved density from the minimum raw density
int CalculateRingDensity::curvedDensityFromMinRawDesity(Vector3 p){
	//int actualDensity;
	double c, r, d;
	int intD;

	r = rawDensityWithRespectToHole(p);
	c = atan(r/5000) * (2/M_PI);
	d = c * 6.0;

	intD = (int)((d > 0.0) ? floor(d + 0.5) : ceil(d - 0.5));

	return intD;
}







