//
//  FleetNameFleetAi.h
//
//  All through this file, you should replace "FleetName",
//    "Fleet Name", and "FLEET_NAME" with the name of your
//     fleet.
//

#ifndef FLEET_NAME_FLEET_AI_H
#define FLEET_NAME_FLEET_AI_H

#include <vector>

#include "ObjLibrary/Vector3.h"


#include "FleetNameUnitAi.h"
#include "PhysicsObjectId.h"
#include "PhysicsObject.h"
#include "ShipAiInterface.h"
#include "Ship.h"
#include "AiShipReference.h"
#include "Marker.h"
#include "FleetAiSuperclass.h"

class WorldInterface;



//
//  FleetName
//
//  A namespace to store classes, functions, and constants for
//    fleet Fleet Name.
//

namespace Apollo_11
{
//
//  FleetAi
//
//  A subclass of FleetAiSuperclass to control the Fleet Name
//    fleet.
//
//  The steps for creating an instance of a FleetAiSuperclass
//    subclass and attaching it to a fleet are as follows:
//    <1> A new instance of the subclass (named p_fas) is
//        created
//    <2> The command ship for the fleet is initialized
//    <3> The fighters for the fleet are initialized
//        sequentially, each using data from the following
//        functions:
//        -> p_fas->getFighterStartPosition
//        -> p_fas->getFighterStartForward
//    <4> The Markers for the fleet are initialized to be
//        stationary and at the origin
//    <5> p_fas->init is called with the ships as parameters.
//        This function should:
//        -> call setShipsAndMarkers
//        -> set the unit AIs for the individual Ships
//    <6> Initialization is complete
//



//class FleetAi : public FleetAiInterface 
	
class FleetAi : public FleetAiSuperclass
{
	public:
	//
	//  Default Constructor
	//
	//  Purpose: To create a new FleetAi without initializing
	//           it.
	//  Parameter(s): N/A
	//  Precondition(s): N/A
	//  Returns: N/A
	//  Side Effect: A new FleetAi is created.
	//

		FleetAi ();

	//
	//  Modified Copy Constructor
	//
	//  Purpose: To create a new FleetAi as a copy of another
	//           uninititialzed FleetAi.  If the other FleetAi
	//           is initialized, use the other modified copy
	//           constructor instead.
	//  Parameter(s): N/A
	//  Precondition(s):
	//    <1> !original.isInitialized()
	//  Returns: N/A
	//  Side Effect: A new FleetAi is created as a deep copy of
	//               original.
	//

		FleetAi (const FleetAi& original);

	//
	//  Modified Copy Constructor
	//
	//  Purpose: To create a new FleetAi that controls the
	//           specified ships as a copy of another
	//           inititialzed FleetAi.  If the other FleetAi is
	//           not initialized, use the other modified copy
	//           constructor instead.
	//  Parameter(s):
	//    <1> original: The FleetAi to copy
	//    <2> command_ship: The command ship to control
	//    <3> v_fighters: An STL vector of the fighters to
	//                    control
	//    <4> vp_markers: An STL vector of pointers to the
	//                    Markers to control
	//  Precondition(s):
	//    <1> original.isInitialized()
	//    <2> command_ship.isShip()
	//    <3> v_fighters.size() == FIGHTER_COUNT
	//    <4> v_fighters[i].isShip()
	//                              WHERE 0 <= i < FIGHTER_COUNT
	//    <5> vp_markers.size() == MARKER_COUNT
	//    <6> vp_markers[i] != NULL
	//                               WHERE 0 <= i < MARKER_COUNT
	//  Returns: N/A
	//  Side Effect: A new FleetAi is created to control Ships
	//               command_ship and v_fighters.  In all other
	//               ways, the new FleetAi is a deep copy of
	//               original.
	//

		FleetAi (const FleetAi& original,
		         AiShipReference command_ship,
		         std::vector<AiShipReference>& v_fighters,
		         std::vector<Marker*>& vp_markers);

	//
	//  Destructor
	//
	//  Purpose: To safely destroy an FleetAi without memory
	//           leaks.
	//  Parameter(s): N/A
	//  Precondition(s): N/A
	//  Returns: N/A
	//  Side Effect: All dynamically allocated memory associated
	//               with this FleetAi is freed.
	//

		virtual ~FleetAi ();

	///////////////////////////////////////////////////////////////
	//
	//  Functions inherited from FleetAiSuperclass
	//

	//
	//  getCloneUninitialized
	//
	//  Purpose: To create a dynamically allocated copy of this
	//           uninitialized FleetAiSuperclass.
	//  Parameter(s): N/A
	//  Precondition(s):
	//    <1> !isInitialized()
	//  Returns: A deep copy of this FleetAiSuperclass is created.
	//  Side Effect: N/A
	//

		virtual FleetAiSuperclass* getCloneUninitialized (
		                                                ) const;

	//
	//  getCloneInitialized
	//
	//  Purpose: To create a dynamically allocated near-copy of this
	//           initialized FleetAiSuperclass.  This near-copy
	//           should be a deep copy of the current
	//           FleetAiSuperclass, except that it should control
	//           the specified Ships and Markers.
	//  Parameter(s):
	//    <1> command_ship: The command ship to control
	//    <2> v_fighters: An STL vector of the fighters to control
	//    <3> vp_markers: An STL vector of pointers to the Markers
	//                    to control
	//  Precondition(s):
	//    <1> isInitialized()
	//    <2> command_ship.isShip()
	//    <3> v_fighters.size() == FIGHTER_COUNT
	//    <4> v_fighters[i].isShip()
	//                                  WHERE 0 <= i < FIGHTER_COUNT
	//    <5> vp_markers.size() == MARKER_COUNT
	//    <6> vp_markers[i] != NULL
	//                                   WHERE 0 <= i < MARKER_COUNT
	//  Returns: A deep near-copy of this FleetAiSuperclass that
	//           controls Ships command_ship, v_fighters, and
	//           Markers v_markers is created.  This near-copy
	//           should be created using a modified copy
	//           constructor for the derived class.
	//  Side Effect: N/A
	//

		virtual FleetAiSuperclass* getCloneInitialized (
		                   AiShipReference command_ship,
		                   std::vector<AiShipReference>& v_fighters,
		                   std::vector<Marker*>& vp_markers) const;

	//
	//  getFighterStartPosition
	//
	//  Purpose: To retrieve the starting position for the specified
	//           fighter.  Note that this function will only be
	//           called before this FleetAiSuperclass is initialized.
	//  Parameter(s):
	//    <1> index: Which fighter
	//  Precondition(s):
	//    <1> index < FIGHTER_COUNT
	//  Returns: The starting position for fighter index in the
	//           local coordinate system for the command ship.  The
	//           local ccordinate system is represented as if the
	//           command ship was at the origin and facing along the
	//           +X-axis, with the +Y-axis as an up vector.
	//  Side Effect: N/A
	//

		virtual Vector3 getFighterStartPosition (
		                              unsigned int index) const;

	//
	//  getFighterStartForward
	//
	//  Purpose: To retrieve the starting forward direction for the
	//           specified fighter.  The fighter will start moving
	//           in this direction at its maximum speed.  Note that
	//           this function will only be called before this
	//           FleetAiSuperclass is initialized.
	//  Parameter(s):
	//    <1> index: Which fighter
	//  Precondition(s):
	//    <1> index < FIGHTER_COUNT
	//  Returns: The starting forward direction for fighter index in
	//           the local coordinate system for the command ship.
	//           The local ccordinate system is represented as if
	//           the command ship was at the origin and facing along
	//           the +X-axis, with the +Y-axis as an up vector.  The
	//           vector returned is always a normal vector.
	//  Side Effect: N/A
	//

		virtual Vector3 getFighterStartForward (
		                              unsigned int index) const;

	//
	//  draw
	//
	//  Purpose: To display the current state of this
	//           FleetAiSuperclass.  This function is intended for
	//           use in debugging and can do nothing.
	//  Parameter(s): N/A
	//  Precondition(s):
	//    <1> isInitialized()
	//  Returns: N/A
	//  Side Effect: Visual information for this FleetAiSuperclass is
	//               displayed.  The default implementation for this
	//               function does nothing.
	//

	//	virtual void draw () const;

	//
	//  init
	//
	//  Purpose: To initialize this FleetAiSuperclass to control the
	//           specified fleet composed of the specified Ships and
	//           controlling the specified Markers, and to assign
	//           unit AIs to the specified Ships.
	//  Parameter(s):
	//    <1> world: The World that the fleet is in
	//    <2> fleet: The identifier for the fleet this
	//               FleetAiSuperclass is to control
	//    <3> command_ship: The command ship to control
	//    <4> v_fighters: An STL vector of the fighters to control
	//    <5> vp_markers: An STL vector of pointers to the Markers
	//                    to control
	//  Precondition(s):
	//    <1> !isInitialized()
	//    <2> fleet <= PhysicsObjectId::FLEET_MAX
	//    <3> command_ship.isShip()
	//    <4> v_fighters.size() == FIGHTER_COUNT
	//    <5> v_fighters[i].isShip()
	//                                  WHERE 0 <= i < FIGHTER_COUNT
	//    <6> vp_markers.size() == MARKER_COUNT
	//    <7> vp_markers[i] != NULL
	//                                   WHERE 0 <= i < MARKER_COUNT
	//  Returns: N/A
	//  Side Effect: This FleetAiSuperclass is initialized to control
	//               fleet fleet, with command ship command_ship,
	//               the the fighters in v_fighters, and the Markers
	//               in vp_markers.  A unit AI is created for and
	//               assigned to the command ship and each of the
	//               fighters.
	//

		virtual void init (const WorldInterface& world,
		                   unsigned int fleet,
		                   AiShipReference command_ship,
		                   std::vector<AiShipReference>& v_fighters,
		                   std::vector<Marker*>& vp_markers);

	//
	//  run
	//
	//  Purpose: To run this FleetAiSuperclass for one frame.  This
	//           function is responsible for running the ship AIs
	//           for the Ships in its fleet.
	//  Parameter(s):
	//    <1> world: The World that the fleet is in
	//  Precondition(s):
	//    <1> isInitialized()
	//  Returns: N/A
	//  Side Effect: The AI for the controlled fleet is run for one
	//               frame.
	//

		virtual void run (const WorldInterface& world);

	private:
	///////////////////////////////////////////////////////////////
	//
	//  Helper function not inherited from anywhere
	//

	//
	//  Assignment Operator
	//
	//  This functions has intentionally not been implemented.
	//    Use the modified copy constructors instead.
	//

		FleetAi& operator= (const FleetAi& original);

	};



}  // end of namespace FleetName



#endif
