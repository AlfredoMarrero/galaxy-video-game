

#include "Ship.h"
#include <cassert>
#include "GetGlut.h"
#include "Ship.h"
#include <cassert>
#include "GetGlut.h"

#include "ObjLibrary/Vector3.h"
#include "ObjLibrary/DisplayList.h"

#include "TimeSystem.h"
#include "WorldInterface.h"
#include "PhysicsObject.h"
#include "UnitAiSuperclass.h"
const double Ship :: RADIUS   =    0.0;
const double Ship :: LIFESPAN =    3.0;
const double Ship :: SPEED    = 1500.0;
const float  Ship :: HEALTH_DEAD_AT = 0.001f;
const int EXPLOSION_SIZE =  20;





Ship::Ship() : PhysicsObject(RADIUS), 
	shipID(PhysicsObjectId::ID_NOTHING), 
	isdead(false), reloadTimer(0.0)
{
	unitAI = NULL;
	acceleration = 250.0;
	maximum_speed = 250.0;
	rotation_rate = (1.0 / 3.0) * 3.14;

}

Ship::Ship(const PhysicsObjectId& id, const Vector3& position, const Vector3& forward,
		   const DisplayList& display_list, double display_scale, float health,
		   int ammo, double speedMax,
		   double acceleration,
		   double rotationRate)
		   : PhysicsObject(id,
		   position,
		   RADIUS,
		   forward * SPEED,
		   display_list,
		   display_scale),
		   creationtime(TimeSystem::getFrameStartTime()),
		   isdead(false),
		   s_health(health),
		   reloadTimer(0.0),
		   acceleration(acceleration),
		   maximum_speed(speedMax),
		   rotation_rate(rotationRate)


{
	assert(id != PhysicsObjectId::ID_NOTHING);
	assert(display_list.isReady());
	assert(display_scale >= 0.0);
}



Ship::Ship(const Ship& original) : PhysicsObject(original), s_health(original.s_health), shipID(original.shipID),
	creationtime(original.creationtime), isdead(original.isdead),
	reloadTimer(original.reloadTimer),acceleration(original.acceleration),
	maximum_speed(original.maximum_speed),
	rotation_rate(original.rotation_rate)
{
}


//Assignment operator;
Ship& Ship::operator= (const Ship& original)
{
	if (&original != this)
	{
		PhysicsObject::operator=(original);

		s_health = original.s_health;
		reloadTimer = original.reloadTimer;
		ammoCount = original.ammoCount;
		isdead = original.isdead;
	}
	//assert(invariant());
	return *this;
}




//Destructor
Ship::~Ship(){}



void Ship::reduceHealth(int amount){

	s_health = s_health - amount;
}


// get health
float Ship::getHealth() const
{
	return s_health;
}

// isreloaded 
bool Ship::isReloaded () const
{

	if(reloadTimer >= 0.25){
		return true;
	}
	else	
		return false;

}

int Ship::getAmmo() const
{
	return ammoCount;
}

// set up camera for ship
void Ship::setupCamera() const
{
	Vector3 forward = getForward();
	Vector3 up = getUp();
	Vector3 position = getPosition() + (20.0 * up) - (100 * forward);
	Vector3 look_at = position + forward;
	gluLookAt(position.x, position.y, position.z,
		look_at.x, look_at.y, look_at.z,
		up.x, up.y, up.z);

}

// get camera for world
CoordinateSystem Ship::getCameraCoordinateSystem() const
{
	Vector3 up = getUp();
	Vector3 forward = getForward();
	Vector3 position = (getPosition() - (100.0* forward) + (20.0 * up));

	CoordinateSystem CameraCoordinateSystem(position, forward, up);


	return CameraCoordinateSystem;
}
//set health
void Ship::setHealth(float health)
{
	s_health = health;
	if (s_health> HEALTH_DEAD_AT)
		isdead = false;

	//cout<<"isdead from setHealth "<<isdead<<endl;
}
// mark reloading
void Ship::markReloading()
{
	reloadTimer = 0.0;
}
// not reloading
void Ship::markNotReloading()
{
	reloadTimer = 0.25;
}

void Ship::setAmmo(int ammo)
{
	ammoCount = ammo;
}

void Ship::addAmmo(int increase)
{
	ammoCount += increase;
	if (ammoCount < 0)
		ammoCount = 0;
}

PhysicsObject* Ship::getClone() const
{
	return new Ship(*this);
}

bool Ship::isAlive() const
{
	//cout<<"isdead in Ship.cpp "<< isdead<< endl;
	return !isdead;
}


// check if helath is low of dying
bool Ship::isDying() const
{
	if (s_health <= 0.5) {
		return true;
	}
	else {

		return false;
	}
}
void Ship::markDead(bool instant)
{
	s_health = 0.0;

	if (instant)
	{
		isdead = true;
	}

	//cout<<"isdead from markDead  "<<isdead <<endl;
}

void Ship::update(WorldInterface& r_world)
{

	//cout<<"updateBasic in ship.cpp"<< getId() <<"     "<<DesiredVelocity<< endl;
	// explode if out of lifespan (or markDead(false) was called)
	if (isAlive() && isDying()){
		r_world.addExplosion(getPositionPrevious(), EXPLOSION_SIZE, 0);
		isdead = true;
		return;
	}

	//cout<<"isdead from update  "<< isdead<< endl;
	if (isUnitAiSet() && isAlive())
	{
		//cout << " desire velocity" << DesiredVelocity << endl;
		double d = DesiredVelocity.getNorm();
		double s = getSpeed();
		double newspeed;

		double deltaS = acceleration *TimeSystem::getFrameDuration();
		//cout << "deltaS" << deltaS << endl;

		if (d > s){
			newspeed = min(s + deltaS,d);
		}
		else{
			newspeed = max( s - deltaS, d);
		}

		Vector3 desired_direction;
		if (DesiredVelocity != Vector3::ZERO)
		{
			setSpeed(newspeed);
			desired_direction = DesiredVelocity.getNormalized();
			double maxrotate = rotation_rate * TimeSystem::getFrameDuration();
			if (d != 0&& desired_direction != Vector3::ZERO)
			{
				rotateTowards(desired_direction, maxrotate);
			}
		}

		else{
			setSpeed(0);

		}

		if (fire == true && isReloaded())
		{
			r_world.addBullet(getPosition(), getForward(), getId());
			fire = false;
			markReloading();
		}
	}

	updateBasic();  
	reloadTimer += TimeSystem::getFrameDuration();
}


double Ship::getRotationRate() const
{
	return rotation_rate;
}


void Ship::setManeuverability(double speed_max,
							  double acceleration,
							  double rotation_rate)
{
	acceleration = acceleration;
	rotation_rate = rotation_rate;
	maximum_speed = speed_max;
}



void Ship::drawUnitAi() const
{

}



void Ship::setDesiredVelocity(const Vector3& desired_velocity)
{
	assert(isUnitAiSet());
	assert(desired_velocity.isNormLessThan(getSpeedMax()));

	DesiredVelocity = desired_velocity;
}


void Ship::markFireBulletDesired()
{
	assert(isUnitAiSet());
	fire = true;
}


void Ship::markFireMissileDesired(
	const PhysicsObjectId& id_target)
{
	assert(isUnitAiSet());
	assert(id_target != PhysicsObjectId::ID_NOTHING);
}


void Ship::runAi(const WorldInterface& world)
{
	assert(isUnitAiSet());
	unitAI->run(world);
}



void Ship::setUnitAi(UnitAiSuperclass* p_unit_ai)
{
	assert(p_unit_ai != NULL);
	assert(p_unit_ai->isControlledShip(*this));
	unitAI = p_unit_ai;

}



void Ship::setUnitAiNone()
{
	unitAI = NULL;
}


double Ship::getSpeedMax() const
{
	return maximum_speed;
}


double Ship::getAcceleration() const
{
	return acceleration;
}




bool Ship::isUnitAiSet() const
{
	if (unitAI != NULL){
		return true;
	}
	return false;
}


UnitAiSuperclass& Ship::getUnitAi() const
{
	assert(isUnitAiSet());
	return* unitAI;

}
