//
// ALfredo Marrero
// 200 330 199
// interface for class CoordinateSystem
// DrawRingParticles.h

#include "ObjLibrary/Vector3.h"
#include "ObjLibrary/ObjModel.h"
#include "GetGlut.h"
#include "ObjLibrary/DisplayList.h"
#include <math.h>
#include <iostream>
#include "CalculateRingDensity.h"
#include "GeometricCollisions.h"

using namespace std;

// preventing multiple declarations
#ifndef DRAW_RING_PARTICLES_H
#define DRAW_RING_PARTICLES_H


struct RingParticle{
	ObjModel particleObj;
	double radius;
};

struct sectorID{
	int x;
	int y;
	int z;
};

struct sector{
	int x;
	int y;
	int z;
};


struct Particles{
       int x, y, z, radius;
};

const int NumberOfParticles = 12;
// class to draw the ring particles
class DrawRingParticles{

public:
	// construtor
	DrawRingParticles();

	// functions
	void drawParticle();
    sectorID getSectorID(Vector3 position);
	Vector3 getPosition_CenterOfSector(sectorID sectorID);
	void initializeSector(sectorID sectorID,  DisplayList particlesList);
	void drowCameraNewarBySectors(Vector3 cameraPosition, DisplayList particlesList);
	void generateWorleyPoints(Vector3 point, int m);
	Vector3 getSectorOrigin(Vector3 vectorID);
	unsigned int pseudorandomValue(Vector3 values);
	unsigned int xorshift (unsigned int a);
	// collision check
	bool collisionWithRingParticles(Vector3 centerObj, double radiusObj);


private:
	//member variables
	ObjModel particle1;
     RingParticle particle;
	sectorID sectorId;
	CalculateRingDensity ringDensity;
	int size;
	int counter;
	Particles particles[NumberOfParticles];
};

#endif