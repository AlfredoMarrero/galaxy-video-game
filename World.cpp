//
//  WorldExplosions.cpp
//

#include <cassert>
#include <iostream>
#include "ObjLibrary/Vector3.h"
#include "PhysicsObjectId.h"
#include "PhysicsObject.h"
#include "ExplosionManagerInterface.h"
#include "ExplosionManager.h"
#include "WorldInterface.h"
#include "World.h"

using namespace std;



World:: World()
		: mp_explosion_manager(new ExplosionManager())
{
		next_bullet = 0;
	assert(invariant());

}

World:: World(const World& original)
		: mp_explosion_manager(original.mp_explosion_manager->getClone())
{
	assert(invariant());
	next_bullet = 0;
}

World:: ~World()
{
	delete mp_explosion_manager;
}

World& World:: operator= (const World& original)
{
	if(&original != this)
	{
		delete mp_explosion_manager;

		mp_explosion_manager = original.mp_explosion_manager->getClone();
	}

	assert(invariant());
	return *this;
}



bool World:: isInitialized () const
{
	assert(mp_explosion_manager != NULL);
	return mp_explosion_manager->isInitialized();
}





void World:: init ()
{
	if(!isInitialized())
	{
		assert(mp_explosion_manager != NULL);
		mp_explosion_manager->init("Explode1.bmp", 15);
	}
	assert(invariant());


	initPlanetSaturn();
	initRings();
	initPlanetoidsShipsBullets();
	initRingParticles();
	initSkybox();

	for(int i = FIRST_FLEET; i <NUMBER_OF_FLEETS; i ++){
		fleets[i].initShips(i, *this);
	}
}

void World:: reset ()
{
	assert(isInitialized());

	assert(mp_explosion_manager != NULL);
	mp_explosion_manager->removeAll();

	assert(invariant());
}

void World:: updateAll ()
{
	assert(isInitialized());

	assert(mp_explosion_manager != NULL);
	mp_explosion_manager->update();




	if(player_ship.isAlive()){
		player_ship.update(*this);
	}


	for(int i = 0; i < numberOfBullets; i++){
		if(bullet[i].isAlive())
			bullet[i].update(*this);
	}


	for(int i = FIRST_FLEET; i< NUMBER_OF_FLEETS; i++){
			 fleets[i].update(*this);
	}

	collisionHandling();
}


///////////////////////////////////////////////////////////////
//
//  Virtual functions inherited from WorldInterface
//


// Determines the desnity of the ring at a position. This is the theoretical
//density at the exact XYZ coordinates, not the density of the sector
//
double World:: getRingDensity (const Vector3& position) const
{
   
	CalculateRingDensity ringDensity;
	return double(ringDensity.curvedDensityFromMinRawDesity(position));
}


// Retreives an STL vector of all ring particles within or intersecting a 
//sphere of radius sphere_radius centered on position sphere_center.
//
vector<WorldInterface::RingParticleData> World:: getRingParticles (const Vector3& sphere_center,
                                                                              double sphere_radius) const 
{
	assert(sphere_radius >= 0.0);
	return vector<RingParticleData>();
}

unsigned int World:: getFleetCount () const
{
	return NUMBER_OF_FLEETS;
}

float World:: getFleetScore (unsigned int fleet) const
{


	cout << "Error: WorldExplosions::getFleetScore is not implemented" << endl;
	assert(fleet <  getFleetCount());
	assert(fleet != PhysicsObjectId::FLEET_NATURE);

	return 0;
}

bool World:: isFleetAlive (unsigned int fleet) const
{

	cout << "Error: WorldExplosions::isFleetAlive is not implemented" << endl;
	assert(fleet <  getFleetCount());
	assert(fleet != PhysicsObjectId::FLEET_NATURE);

	return false;
}

PhysicsObjectId World:: getFleetCommandShipId (unsigned int fleet) const
{
	assert(fleet <  getFleetCount());
	assert(fleet != PhysicsObjectId::FLEET_NATURE);

	return fleets[fleet].getCommandShipId();
}

vector<PhysicsObjectId> World:: getFleetFighterIds (unsigned int fleet) const
{
	cout << "Error: WorldExplosions::getFleetFighterIds is not implemented" << endl;
	assert(fleet <  getFleetCount());
	assert(fleet != PhysicsObjectId::FLEET_NATURE);

	return vector<PhysicsObjectId>();
}

vector<PhysicsObjectId> World:: getFleetMissileIds (unsigned int fleet) const
{
	cout << "Error: WorldExplosions::getFleetMissileIds is not implemented" << endl;
	assert(fleet <  getFleetCount());
	assert(fleet != PhysicsObjectId::FLEET_NATURE);

	return vector<PhysicsObjectId>();
}

PhysicsObjectId World:: getPlanetId () const
{
	return planetoidSaturn.getId();
}

unsigned int World:: getMoonCount () const
{
	return NumberOfMoons;
}

PhysicsObjectId World:: getMoonId (unsigned int moon) const
{
	assert(moon < getMoonCount());
	return planetoidMoon[moon].getId();
}


//Returns the id of the planet or moon closest to position
PhysicsObjectId World:: getNearestPlanetoidId (const Vector3& position) const
{

	int index = 0;
	double minDistance = position.getDistanceSquared(Vector3(moons[index].x, 0.0, moons[index].z));
	for(int i = 1; i <  NumberOfMoons; i++){
	
		double newDistance = position.getDistanceSquared(planetoidMoon[i].getPosition());
		if(minDistance > newDistance){
			minDistance = newDistance;
			index = i;
		}
	}
	
	if(minDistance > position.getDistanceSquared(planetoidSaturn.getPosition())){
		return planetoidSaturn.getId();
	}

	return planetoidMoon[index].getId();
}

//Returns an STL vector of the ids of all Ships within or intersecting
//a sphere of radius sphere_radius centered on position sphere_center.
vector<PhysicsObjectId> World:: getShipIds (const Vector3& sphere_center,
											double sphere_radius) const
{
	//cout << "Error: WorldExplosions::getShipIds is not implemented" << endl;
	assert(sphere_radius >= 0.0);

	vector<PhysicsObjectId> shipsIds;

	if(player_ship.getPosition().getDistance(sphere_center) < player_ship.getRadius() + sphere_radius&& player_ship.isAlive()){

		shipsIds.push_back(player_ship.getId());
	}

	for(int i = FIRST_FLEET; i< NUMBER_OF_FLEETS; i++){
		
		for(int j = 0; j < 13; j++){
			Vector3 shipPosition = fleets[i].getShip(j).getPosition();
			double shipRadius = fleets[i].getShip(j).getRadius();
			bool shipIsAlive = fleets[i].getShip(j).isAlive();



			if(shipPosition.getDistance(sphere_center) < shipRadius + sphere_radius && shipIsAlive){
		
			shipsIds.push_back( fleets[i].getShip(j).getId());
			
			}
		
		}

	}

	return shipsIds;
}


//     Returns whether the PhysicsObject with id id is alive
//
//This function must return true when any other query function is called for the same id.
//If this function would return false, calling the other function will generate an error.
//
//
bool World:: isAlive (const PhysicsObjectId& id) const
{
	if (id == PhysicsObjectId::ID_NOTHING){
		return false;
	}

	if (id.m_type ==PhysicsObjectId::TYPE_PLANETOID &&id.m_fleet ==PhysicsObjectId::FLEET_NATURE && id.m_index == 12)
	{
		return planetoidSaturn.isAlive ();
	}
	else if(id.m_type == PhysicsObjectId::TYPE_PLANETOID && id.m_fleet == PhysicsObjectId::FLEET_NATURE)
	{
		return planetoidMoon[id.m_index].isAlive ();
	}
	else if (id.m_type == PhysicsObjectId::TYPE_SHIP && id.m_fleet == PhysicsObjectId::FLEET_PLAYER && id.m_index == 251)
	{
		return player_ship.isAlive ();
	}
	else if(id.m_fleet >= PhysicsObjectId::FLEET_ENEMY){
	
		return fleets[id.m_fleet].getShip(id.m_index).isAlive();
	}


	else if (id.m_type == PhysicsObjectId::TYPE_BULLET && id.m_fleet ==PhysicsObjectId::FLEET_ENEMY)
	{
		return bullet[id.m_index].isAlive ();
	}
	else if (id.m_type == PhysicsObjectId::TYPE_BULLET && id.m_fleet ==PhysicsObjectId::FLEET_PLAYER)
	{
		return bullet[id.m_index].isAlive ();
	}
	
	return false;
}


//Returns the position of the center of the PhysicsObject with id id
Vector3 World:: getPosition (const PhysicsObjectId& id) const
{

	assert(isAlive(id));

	if (id.m_type == PhysicsObjectId::ID_NOTHING){
		return Vector3::ZERO;
	}
	if (id.m_type ==PhysicsObjectId::TYPE_PLANETOID &&id.m_fleet ==PhysicsObjectId::FLEET_NATURE && id.m_index == 12)
	{
		return planetoidSaturn.getPosition();
	}
	else if(id.m_type == PhysicsObjectId::TYPE_PLANETOID && id.m_fleet == PhysicsObjectId::FLEET_NATURE)
	{
		return planetoidMoon[id.m_index].getPosition();
	}
	else if (id.m_type == PhysicsObjectId::TYPE_SHIP)
	{
		if (id.m_fleet == PhysicsObjectId::FLEET_PLAYER && id.m_index == 251)
		{
			return player_ship.getPosition();
		}
		else if (id.m_fleet >= PhysicsObjectId::FLEET_ENEMY)
		{
			int fleet = id.m_fleet;

			if(!fleets[fleet].getShip(id.m_index).isAlive()){
			      return Vector3::ZERO;
			}

			return fleets[fleet].getShip(id.m_index).getPosition();
		}
	}
	else if (id.m_type == PhysicsObjectId::TYPE_BULLET && id.m_fleet ==PhysicsObjectId::FLEET_ENEMY)
	{
		return bullet[id.m_index].getPosition();
	}
	else if (id.m_type == PhysicsObjectId::TYPE_BULLET && id.m_fleet ==PhysicsObjectId::FLEET_PLAYER)
	{
		return bullet[id.m_index].getPosition();
	}


	return Vector3::ZERO;
}

double World:: getRadius (const PhysicsObjectId& id) const
{
	assert(isAlive(id));

	if (id.m_type == PhysicsObjectId::ID_NOTHING){
		return 0.0;
	}

	if (id.m_type ==PhysicsObjectId::TYPE_PLANETOID && id.m_fleet== PhysicsObjectId::FLEET_NATURE && id.m_index == 12)
	{
		return planetoidSaturn.getRadius();
	}
	else if(id.m_type == PhysicsObjectId::TYPE_PLANETOID && id.m_fleet == PhysicsObjectId::FLEET_NATURE)
	{
		return planetoidMoon[id.m_index].getRadius();
	}
	else if (id.m_type == PhysicsObjectId::TYPE_SHIP && id.m_fleet == PhysicsObjectId::FLEET_PLAYER && id.m_index == 251)
	{
		return player_ship.getRadius();
	}

	else if(id.m_fleet >= PhysicsObjectId::FLEET_ENEMY){

		return fleets[id.m_fleet].getShip(id.m_index).getRadius();
	}
	else if (id.m_type == PhysicsObjectId::TYPE_BULLET && id.m_fleet== PhysicsObjectId::FLEET_ENEMY)
	{
		return bullet[id.m_index].getRadius();
	}
	else if (id.m_type == PhysicsObjectId::TYPE_BULLET && id.m_fleet== PhysicsObjectId::FLEET_PLAYER)
	{
		return bullet[id.m_index].getRadius();
	}



	return 0.0;
}

Vector3 World:: getVelocity (const PhysicsObjectId& id) const
{
	assert(isAlive(id));
	if (id.m_type == PhysicsObjectId::ID_NOTHING){
		return Vector3::ZERO;
	}
		if (id.m_type== PhysicsObjectId::TYPE_PLANETOID && id.m_fleet== PhysicsObjectId::FLEET_NATURE && id.m_index == 12)
	{
		return planetoidSaturn.getVelocity ();
	}
	else if(id.m_type == PhysicsObjectId::TYPE_PLANETOID && id.m_fleet == PhysicsObjectId::FLEET_NATURE)
	{
		return planetoidMoon[id.m_index].getVelocity();
	}
	else if (id.m_type == PhysicsObjectId::TYPE_SHIP && id.m_fleet == PhysicsObjectId::FLEET_PLAYER && id.m_index == 251)
	{
		return player_ship.getVelocity();
	}
	else if(id.m_fleet >= PhysicsObjectId::FLEET_ENEMY && id.m_type ==PhysicsObjectId::TYPE_SHIP){
	
		return fleets[id.m_fleet].getShip(id.m_index).getVelocity();
	}
	else if (id.m_type == PhysicsObjectId::TYPE_BULLET && id.m_fleet== PhysicsObjectId::FLEET_ENEMY)
	{
		return bullet[id.m_index].getVelocity();
	}
	else if (id.m_type == PhysicsObjectId::TYPE_BULLET && id.m_fleet== PhysicsObjectId::FLEET_PLAYER)
	{
		return bullet[id.m_index].getVelocity();
	}

	return Vector3::ZERO;
}

double World:: getSpeed (const PhysicsObjectId& id) const
{
	assert(isAlive(id));

	if (id.m_type == PhysicsObjectId::ID_NOTHING){
		return 0.0;
	}
	if (id.m_type ==PhysicsObjectId::TYPE_PLANETOID && id.m_fleet== PhysicsObjectId::FLEET_NATURE && id.m_index == 12)
	{
		return planetoidSaturn.getSpeed();
	}
	else if(id.m_type == PhysicsObjectId::TYPE_PLANETOID && id.m_fleet == PhysicsObjectId::FLEET_NATURE)
	{
		return planetoidMoon[id.m_index].getSpeed();
	}
	else if (id.m_type == PhysicsObjectId::TYPE_SHIP && id.m_fleet == PhysicsObjectId::FLEET_PLAYER && id.m_index == 251)
	{
		return player_ship.getSpeed();
	}
    else if(id.m_fleet >= PhysicsObjectId::FLEET_ENEMY && id.m_type ==PhysicsObjectId::TYPE_SHIP){
	
		return fleets[id.m_fleet].getShip(id.m_index).getSpeed();
	}
	else if (id.m_type == PhysicsObjectId::TYPE_BULLET && id.m_fleet== PhysicsObjectId::FLEET_ENEMY)
	{
		return bullet[id.m_index].getSpeed();
	}
	else if (id.m_type == PhysicsObjectId::TYPE_BULLET && id.m_fleet== PhysicsObjectId::FLEET_PLAYER)
	{
		return bullet[id.m_index].getSpeed();
	}

	return 0.0;
}

Vector3 World:: getForward (const PhysicsObjectId& id) const
{
	assert(isAlive(id));

	if (id.m_type == PhysicsObjectId::ID_NOTHING){
		Vector3::UNIT_X_PLUS;
	}
	if (id.m_type == PhysicsObjectId::TYPE_PLANETOID && id.m_fleet== PhysicsObjectId::FLEET_NATURE && id.m_index == 12)
	{
		return planetoidSaturn.getForward();
	}
	else if(id.m_type == PhysicsObjectId::TYPE_PLANETOID && id.m_fleet == PhysicsObjectId::FLEET_NATURE)
	{
		return planetoidMoon[id.m_index].getForward();
	}
	else if (id.m_type == PhysicsObjectId::TYPE_SHIP && id.m_fleet == PhysicsObjectId::FLEET_PLAYER && id.m_index == 251)
	{
		return player_ship.getForward();
	}
	else if(id.m_fleet >= PhysicsObjectId::FLEET_ENEMY && id.m_type ==PhysicsObjectId::TYPE_SHIP){
	
		return fleets[id.m_fleet].getShip(id.m_index).getForward();
	}

	else if (id.m_type == PhysicsObjectId::TYPE_BULLET && id.m_fleet== PhysicsObjectId::FLEET_ENEMY)
	{
		return bullet[id.m_index].getForward();
	}
	else if (id.m_type == PhysicsObjectId::TYPE_BULLET && id.m_fleet== PhysicsObjectId::FLEET_PLAYER)
	{
		return bullet[id.m_index].getForward();
	}

	return Vector3::UNIT_X_PLUS;
}

Vector3 World:: getUp (const PhysicsObjectId& id) const
{
	assert(isAlive(id));


	if (id.m_type == PhysicsObjectId::ID_NOTHING){
		Vector3::UNIT_Y_PLUS;
	}
	if (id.m_type == PhysicsObjectId::TYPE_PLANETOID && id.m_fleet== PhysicsObjectId::FLEET_NATURE && id.m_index == 12)
	{
		return planetoidSaturn.getUp ();
	}
	else if(id.m_type == PhysicsObjectId::TYPE_PLANETOID && id.m_fleet == PhysicsObjectId::FLEET_NATURE)
	{
		return planetoidMoon[id.m_index].getUp ();
	}
	else if (id.m_type == PhysicsObjectId::TYPE_SHIP && id.m_fleet == PhysicsObjectId::FLEET_PLAYER && id.m_index == 251)
	{
		return player_ship.getUp ();
	}
	else if(id.m_fleet >= PhysicsObjectId::FLEET_ENEMY && id.m_type ==PhysicsObjectId::TYPE_SHIP){
	
		return fleets[id.m_fleet].getShip(id.m_index).getUp();
	}
	else if (id.m_type == PhysicsObjectId::TYPE_BULLET && id.m_fleet== PhysicsObjectId::FLEET_ENEMY)
	{
		return bullet[id.m_index].getUp ();
	}
	else if (id.m_type == PhysicsObjectId::TYPE_BULLET && id.m_fleet== PhysicsObjectId::FLEET_PLAYER)
	{
		return bullet[id.m_index].getUp ();
	}

	return Vector3::UNIT_Y_PLUS;
}

Vector3 World:: getRight (const PhysicsObjectId& id) const
{
	assert(isAlive(id));
	if (id.m_type == PhysicsObjectId::ID_NOTHING){
		Vector3::UNIT_Z_PLUS;
	}
	if (id.m_type == PhysicsObjectId::TYPE_PLANETOID && id.m_fleet== PhysicsObjectId::FLEET_NATURE && id.m_index == 12)
	{
		return planetoidSaturn.getRight ();
	}
	else if(id.m_type == PhysicsObjectId::TYPE_PLANETOID && id.m_fleet == PhysicsObjectId::FLEET_NATURE)
	{
		return planetoidMoon[id.m_index].getRight();
	}
	else if (id.m_type == PhysicsObjectId::TYPE_SHIP && id.m_fleet == PhysicsObjectId::FLEET_PLAYER && id.m_index == 251)
	{
		return player_ship.getRight();
	}
	else if(id.m_fleet >= PhysicsObjectId::FLEET_ENEMY && id.m_type ==PhysicsObjectId::TYPE_SHIP){
	
		return fleets[id.m_fleet].getShip(id.m_index).getRight();
	}
	else if (id.m_type == PhysicsObjectId::TYPE_BULLET && id.m_fleet== PhysicsObjectId::FLEET_ENEMY)
	{
		return bullet[id.m_index].getRight();
	}
	else if (id.m_type == PhysicsObjectId::TYPE_BULLET && id.m_fleet== PhysicsObjectId::FLEET_PLAYER)
	{
		return bullet[id.m_index].getRight();
	}


	return Vector3::UNIT_Z_PLUS;
}

bool World:: isPlanetoidMoon (const PhysicsObjectId& id) const
{
	assert(id.m_type == PhysicsObjectId::TYPE_PLANETOID);
	assert(isAlive(id));
	

	if(id.m_type == PhysicsObjectId::TYPE_PLANETOID && id.m_fleet== PhysicsObjectId::FLEET_NATURE)
	{
		return true;
	}
	
	return false;
}

// Returns the minimum distance from the surface of planetoid id to the ring
double World:: getPlanetoidRingDistance (const PhysicsObjectId& id) const
{
	assert(id.m_type == PhysicsObjectId::TYPE_PLANETOID);
	assert(isAlive(id));
	assert(isPlanetoidMoon(id));

	if (id.m_type == PhysicsObjectId::TYPE_PLANETOID && id.m_fleet == PhysicsObjectId::FLEET_NATURE && id.m_index == 12)
	{
		return 12000 - planetoidSaturn.getRadius() ;
	}
	else if(id.m_type == PhysicsObjectId::TYPE_PLANETOID && id.m_fleet == PhysicsObjectId::FLEET_NATURE)
	{
		return 12000 - planetoidMoon[id.m_index].getRadius();
	}
	

	return 0.0;
}

unsigned int World:: getPlanetoidOwner (const PhysicsObjectId& id) const
{
	assert(id.m_type == PhysicsObjectId::TYPE_PLANETOID);
	assert(isAlive(id));

	if (id.m_type == PhysicsObjectId::TYPE_PLANETOID && id.m_fleet== PhysicsObjectId::FLEET_NATURE && id.m_index == 12)
	{
		return planetoidSaturn.getOwner ();
	}
	else if(id.m_type == PhysicsObjectId::TYPE_PLANETOID && id.m_fleet == PhysicsObjectId::FLEET_NATURE)
	{
		return planetoidMoon[id.m_index].getOwner();
	}
	
	return PhysicsObjectId::FLEET_NATURE;
}

bool World:: isPlanetoidActivelyClaimed (const PhysicsObjectId& id) const
{
	assert(id.m_type == PhysicsObjectId::TYPE_PLANETOID);
	assert(isAlive(id));

	if (id.m_type == PhysicsObjectId::TYPE_PLANETOID && id.m_fleet== PhysicsObjectId::FLEET_NATURE && id.m_index == 12)
	{
		return planetoidSaturn.isActivelyClaimed ();
	}
	else if(id.m_type == PhysicsObjectId::TYPE_PLANETOID && id.m_fleet == PhysicsObjectId::FLEET_NATURE)
	{
		return planetoidMoon[id.m_index].isActivelyClaimed();
	}

	return false;
}

bool World:: isShipCommandShip (const PhysicsObjectId& id) const
{
	assert(id.m_type == PhysicsObjectId::TYPE_SHIP);
	assert(isAlive(id));

	if(id.m_fleet >= PhysicsObjectId::FLEET_ENEMY && id.m_type ==PhysicsObjectId::TYPE_SHIP && id.m_index == 12){
		return true;
	}

	return false;
}

double World:: getShipSpeedMax (const PhysicsObjectId& id) const
{
	assert(id.m_type == PhysicsObjectId::TYPE_SHIP);
	assert(isAlive(id));

	
	if (id.m_type == PhysicsObjectId::TYPE_SHIP && id.m_fleet == PhysicsObjectId::FLEET_PLAYER && id.m_index == 251)
	{
		return player_ship.getSpeedMax();
	}
   else if(id.m_fleet >= PhysicsObjectId::FLEET_ENEMY && id.m_type ==PhysicsObjectId::TYPE_SHIP){
		return fleets[id.m_fleet].getShip(id.m_index).getSpeedMax();
	}

	return 1.0;
}

double World:: getShipAcceleration (const PhysicsObjectId& id) const
{
	assert(id.m_type == PhysicsObjectId::TYPE_SHIP);
	assert(isAlive(id));

	if (id.m_type == PhysicsObjectId::TYPE_SHIP && id.m_fleet == PhysicsObjectId::FLEET_PLAYER && id.m_index == 251)
	{
		return player_ship.getAcceleration();
	}
	else if(id.m_fleet >= PhysicsObjectId::FLEET_ENEMY && id.m_type ==PhysicsObjectId::TYPE_SHIP){
	
		return fleets[id.m_fleet].getShip(id.m_index).getAcceleration();
	}

	return 0.0;
}

double World:: getShipRotationRate (const PhysicsObjectId& id) const
{
	assert(id.m_type == PhysicsObjectId::TYPE_SHIP);
	assert(isAlive(id));

	if (id.m_type == PhysicsObjectId::TYPE_SHIP && id.m_fleet == PhysicsObjectId::FLEET_PLAYER && id.m_index == 251)
	{
		return player_ship.getRotationRate();
	}
	else if(id.m_fleet >= PhysicsObjectId::FLEET_ENEMY && id.m_type ==PhysicsObjectId::TYPE_SHIP){
	
		return fleets[id.m_fleet].getShip(id.m_index).getRotationRate();
	}



	return 0.0;
}

float World:: getShipHealthCurrent (const PhysicsObjectId& id) const
{
	assert(id.m_type == PhysicsObjectId::TYPE_SHIP);
	assert(isAlive(id));

	if (id.m_type == PhysicsObjectId::TYPE_SHIP && id.m_fleet == PhysicsObjectId::FLEET_PLAYER && id.m_index == 251)
	{
		return player_ship.getHealth();
	}

	else if(id.m_fleet >= PhysicsObjectId::FLEET_ENEMY && id.m_type ==PhysicsObjectId::TYPE_SHIP){
	
		return fleets[id.m_fleet].getShip(id.m_index).getHealth();
	}




	return 0.0f;
}

float World:: getShipHealthMaximum (const PhysicsObjectId& id) const
{
	assert(id.m_type == PhysicsObjectId::TYPE_SHIP);
	assert(isAlive(id));
	return 1.0f;
}

bool World:: isMissileOutOfFuel (const PhysicsObjectId& id) const
{
	cout << "Error: WorldExplosions::isMissileOutOfFuel is not implemented" << endl;
	assert(id.m_type == PhysicsObjectId::TYPE_MISSILE);
	assert(isAlive(id));

	return 0.0f;
}

PhysicsObjectId World:: getMissileTarget (const PhysicsObjectId& id) const
{
	cout << "Error: WorldExplosions::getMissileTarget is not implemented" << endl;
	assert(id.m_type == PhysicsObjectId::TYPE_MISSILE);
	assert(isAlive(id));

	return PhysicsObjectId::ID_NOTHING;
}



void World:: addExplosion (const Vector3& position,
                                      double size,
                                      unsigned int type)
{
	assert(size >= 0.0);

	mp_explosion_manager->add(position, size);

	assert(invariant());
}




//Adds a bullet to the world at position position moving in direction forward. 
//The bullet is marked as having been fired by the object with id source_id
PhysicsObjectId World:: addBullet (const Vector3& position,
                                              const Vector3& forward,
                                              const PhysicsObjectId& source_id)
{
	assert(forward.isNormal());
	bullet[next_bullet].fire(position, forward, source_id);
	PhysicsObjectId bullet_id(PhysicsObjectId::TYPE_BULLET,PhysicsObjectId::FLEET_ENEMY,next_bullet);
   next_bullet = (next_bullet + 1)  % numberOfBullets;
	assert(invariant());
	return bullet_id;

}

PhysicsObjectId World:: addMissile (const Vector3& position,
                                               const Vector3& forward,
                                               const PhysicsObjectId& source_id,
                                               const PhysicsObjectId& target_id)
{
	cout << "Error: WorldExplosions::addMissile is not implemented" << endl;
	assert(forward.isNormal());

	assert(invariant());
	return PhysicsObjectId::ID_NOTHING;
}



///////////////////////////////////////////////////////////////
//
//  Helper function not inherited from anywhere
//

bool World:: invariant () const
{
	if(mp_explosion_manager == NULL) return false;
	return true;
}



void World:: draw (const Vector3& camera_forward,
                              const Vector3& camera_up) const
{
	assert(isInitialized());
	assert(camera_forward.isNormal());
	assert(camera_up.isNormal());
	assert(camera_forward.isOrthogonal(camera_up));

	assert(mp_explosion_manager != NULL);
	mp_explosion_manager->draw(camera_forward, camera_up);


	// draw Staturn
	planetoidSaturn.draw();

    //draw moons
	for(int i = 0; i < NumberOfMoons; i++){
		planetoidMoon[i].draw();
	}

	// draw ships
	if(player_ship.isAlive()){
		player_ship.draw();
	}

	// draw bullets
	for(int i = 0; i < numberOfBullets; i++){
		if(bullet[i].isAlive())
			bullet[i].draw();
	}


	for(int i = FIRST_FLEET; i < NUMBER_OF_FLEETS; i++){
		fleets[i].draw();
	}

 
}


//////////////////////////////////////////////////////////////
//
//FUNCTION TO DISPLY THE OBJECTS IN THE WORLD AND MANIPULATE THEM 
//


void World::initPlanetSaturn(){

	ObjModel planetSaturn;
	planetSaturn.load("Models/Saturn.obj");

	planetSaturnDL= planetSaturn.getDisplayList();

	// plannet saturn
	PhysicsObjectId planetID = PhysicsObjectId (PhysicsObjectId::TYPE_PLANETOID,PhysicsObjectId::FLEET_NATURE, 12);
	Vector3 position;
	position.x = 0.0;
	position.y = 0.0;
	position.z = 0.0;
	planetoidSaturn.initPlanetoid(planetID, position, 60000.0, planetSaturnDL, 60000.0);

}

Ship& World::getPlayerShip(){

	return player_ship;

}



void World::initPlanetoidsShipsBullets(){

	ObjModel MoonA;
	ObjModel MoonB;
	ObjModel MoonC;
	ObjModel MoonD;
	ObjModel MoonE;
	ObjModel MoonF;
	ObjModel MoonG;
	ObjModel MoonH;
	ObjModel MoonI;
	ObjModel MoonJ;
	ObjModel shipObjModel;


	// loading moons, planet, rings and skybox
	MoonA.load("Models/MoonA.obj");//
	MoonB.load("Models/MoonB.obj");
	MoonC.load("Models/MoonC.obj");
	MoonD.load("Models/MoonD.obj");
	MoonE.load("Models/MoonE.obj");
	MoonF.load("Models/MoonF.obj");
	MoonG.load("Models/MoonG.obj");
	MoonH.load("Models/MoonH.obj");
	MoonI.load("Models/MoonI.obj");
	MoonJ.load("Models/MoonJ.obj");

	//moonA
	moons[0].moonModelDL = MoonA.getDisplayList();
	moons[0].x = 0.0;
	moons[0].z = 140000.0;
	moons[0].radius =3300.0;
	//moonB
	moons[1].moonModelDL =  MoonB.getDisplayList();
	moons[1].radius = 4300.0;
	moons[1].x = 85000.0;
	moons[1].z = 75000.0;
	//moonC
	moons[2].moonModelDL =  MoonC.getDisplayList();
	moons[2].radius = 2000.0;
	moons[2].x = 130000.0;
	moons[2].z = 40000.0;
	//moonD
	moons[3].moonModelDL = MoonD.getDisplayList();
	moons[3].radius = 3400.0;
	moons[3].x = 110000.0;
	moons[3].z = -60000.0;
	//moonE
	moons[4].moonModelDL =  MoonE.getDisplayList();
	moons[4].radius = 5000.0;
	moons[4].x = 100000.0;
	moons[4].z = -70000.0;
	//moonF
	moons[5].moonModelDL =  MoonF.getDisplayList();
	moons[5].radius = 3100.0;
	moons[5].x = 20000.0;
	moons[5].z = -135000.0;
	//moonG
	moons[6].moonModelDL =  MoonG.getDisplayList();
	moons[6].radius = 2600;
	moons[6].x = -60000;
	moons[6].z = -80000;
	//moonH
	moons[7].moonModelDL =  MoonH.getDisplayList();
	moons[7].radius = 2200.0;
	moons[7].x = -95000.0;
	moons[7].z = -70000.0;
	//moonI
	moons[8].moonModelDL =  MoonI.getDisplayList();
	moons[8].radius = 4700.0;
	moons[8].x = -90000.0;
	moons[8].z = -40000.0;
	//moonJ
	moons[9].moonModelDL =  MoonJ.getDisplayList();
	moons[9].radius = 3800.0;
	moons[9].x = -100000.0;
	moons[9].z = 50000.0;

	coordinateSystem.setPosition();
	shipObjModel.load("Models/Grapple.obj");
	shipDL = shipObjModel.getDisplayList();

	  
	// MOONS 
	Vector3 moonPosition;
	for(int i = 0; i < NumberOfMoons; i++){
		PhysicsObjectId moonID = PhysicsObjectId (PhysicsObjectId::TYPE_PLANETOID, PhysicsObjectId::FLEET_NATURE, i);
		planetoidMoon[i].initPlanetoid(moonID, Vector3(moons[i].x, 0.0, moons[i].z),
			moons[i].radius, moons[i].moonModelDL, moons[i].radius);
	}

	planetoidMoon[2];


	//PLAYER SHIP
	Vector3 playerPosition;
	playerPosition.x =	moons[8].x;
	playerPosition.y = 20000.0;
	//playerPosition.y = moons[8].radius + 500;
	playerPosition.z =moons[8].z;
	PhysicsObjectId playerID = PhysicsObjectId (PhysicsObjectId::TYPE_SHIP,PhysicsObjectId::FLEET_PLAYER, 251);
	Vector3 playerShipVelocity = SPEED * player_ship.getForward();
	player_ship.initPhysics(playerID, playerPosition, 10.0, playerShipVelocity, shipDL, 10.0);
	player_ship.setHealth(10.0);
	player_ship.setAmmo(8.0);


	//BULLETS
	ObjModel bulletObj;
	DisplayList bulletDL;
	bulletObj.load("Models/Bolt.obj");
	bulletDL = bulletObj.getDisplayList();

	for(int j = 0; j < numberOfBullets; j++){
		PhysicsObjectId bulletID = PhysicsObjectId (PhysicsObjectId::TYPE_BULLET,PhysicsObjectId::FLEET_PLAYER, j);
		bullet[j].initPhysics(bulletID, playerPosition, 0.0, Vector3(0,0,0), bulletDL, 2.5);
	}



}



void World::playerShipFireBullet(){

	if(player_ship.isAlive()&& player_ship.isReloaded()){
		bullet[next_bullet].fire(player_ship.getPosition(), player_ship.getForward(), player_ship.getId());

		player_ship.markReloading();
		if (bullet[next_bullet].isAlive()){
			next_bullet = next_bullet + 1;
		}

		if(next_bullet == numberOfBullets){
			next_bullet = 0;
		}
	}

}



////////////////////////////////////////////////////////

void World::drawRingParticles(){

	DrawRingParticles drawSector;
	drawSector.drowCameraNewarBySectors(player_ship.getPosition(), particlesList);
}



void World::initRingParticles(){

	ObjModel particles;
	particles.load("Models/RingParticleA0.obj");
    particlesList = particles.getDisplayList();

}






void World::initRings(){
	ObjModel ring;
	ring.load("Models/ring.obj");
	ringDL = ring.getDisplayList();

}
void World::drawRings(){

	glPushMatrix();
		glTranslated(0.0, 0.0, 0.0);
		glScaled(60000.0, 60000.0, 60000.0);
		ringDL.draw();
	glPopMatrix();
}

void World::initSkybox(){
	ObjModel skybox;
	skybox.load("Models/Skybox.obj");
	skyboxDL = skybox.getDisplayList();

}
void World::drawSkybox(){

		// drawing the skybox
	glDisable(GL_DEPTH_TEST);
	glPushMatrix();
		glTranslated(player_ship.getPosition().x,player_ship.getPosition().y, player_ship.getPosition().z);
		glScaled(100000.0, 100000.0, 100000.0);
		glDepthMask(GL_FALSE);
		skyboxDL.draw();
		glDepthMask(GL_TRUE);
	glPopMatrix();
	glEnable(GL_DEPTH_TEST);
}





//Create an overall collision handling function and call it after all physics updates are completed. 
//This overall function should call a collision handling function for each ship, then for each bullet,


void World::collisionHandling(){

	if(player_ship.isAlive()){
		
		collisionHandlingForPlayerShip(player_ship);

		// handling collision between the player ship and ring particles
		if(collisionCheck.collisionWithRingParticles(player_ship.getPosition(), player_ship.getRadius())){
			player_ship.markDead(false);
		}
	}

	for(int i = FIRST_FLEET; i < NUMBER_OF_FLEETS; i++){
			//collisionHandlingForShip(ship[i], ship[i].getPosition(), ship[i].getRadius(), i);
		for(int j = 0; j < 13; j++){
			collisionHandlingForShip(fleets[i].getShip(j), i);
		}
	}

	// handling collision for bullets
	for(int i = 0; i < numberOfBullets; i++){
		if(bullet[i].isAlive()){
           collisionHandlingForBullet(bullet[i].getPosition(), i);
		}
	}
}




//The collision handling function for a single ship should start by handling collisions with planetoids. 
//Then it should handle any collisions with ships with larger ids.  If you also detect collisions with 
//ships with smaller ids, you will detect each collision twice.  If you detect a collision with a ship 
//with the same id, the ship will collide with itself.
//void collisionHandlingForShip(Ship& ship, Vector3 shipPosition, double shipRadius, int n){
void World::collisionHandlingForPlayerShip(Ship& shipObj){

	//collision handling function for a single ship should start by handling collisions with planetoids
	for (int i = 0; i < NumberOfMoons; i ++){

		if(GeometricCollisions::sphereVsSphere(shipObj.getPosition(), shipObj.getRadius(), planetoidMoon[i].getPosition(), planetoidMoon[i].getRadius()))
		{
			shipObj.markDead (false);
		}
	}

	// hadling collision with Saturn 
	if (GeometricCollisions::sphereVsSphere(shipObj.getPosition(), shipObj.getRadius(), 
										planetoidSaturn.getPosition(),
										planetoidSaturn.getRadius()))
	{
		shipObj.markDead (false);
	}

	//Then it should handle any collisions with ships with larger ids.
	for(int i = FIRST_FLEET; i < NUMBER_OF_FLEETS; i++){
			//collisionHandlingForShip(ship[i], ship[i].getPosition(), ship[i].getRadius(), i);
		for(int j = 0; j < 13; j++){


				if(fleets[i].getShip(j).isAlive() && GeometricCollisions::sphereVsSphere(shipObj.getPosition(), shipObj.getRadius(), 
					fleets[i].getShip(j).getPosition(), fleets[i].getShip(j).getRadius()))
				{
						shipObj.markDead (false);
						fleets[i].getShip(j).markDead (false);
				}
		}
	}
}




//The collision handling function for a single ship should start by handling collisions with planetoids. 
//Then it should handle any collisions with ships with larger ids.  If you also detect collisions with 
//ships with smaller ids, you will detect each collision twice.  If you detect a collision with a ship 
//with the same id, the ship will collide with itself.
//void collisionHandlingForShip(Ship& ship, Vector3 shipPosition, double shipRadius, int n){
void World::collisionHandlingForShip(Ship& shipObj, int n){


	//collision handling function for a single ship should start by handling collisions with planetoids
	for (int i = 0; i < NumberOfMoons; i ++){

		if(GeometricCollisions::sphereVsSphere(shipObj.getPosition(), shipObj.getRadius(), planetoidMoon[i].getPosition(), planetoidMoon[i].getRadius()))
		{
			shipObj.markDead (false);
		}
	}

	// hadling collision with Saturn 
	if (GeometricCollisions::sphereVsSphere(shipObj.getPosition(), shipObj.getRadius(), 
										planetoidSaturn.getPosition(),
										planetoidSaturn.getRadius()))
	{
		shipObj.markDead (false);
	}

	//Then it should handle any collisions with ships with larger ids.
	for(int i = FIRST_FLEET; i < NUMBER_OF_FLEETS; i++){
			//collisionHandlingForShip(ship[i], ship[i].getPosition(), ship[i].getRadius(), i);
		for(int j = 0; j < 13; j++){


			if(shipObj.getId() < fleets[i].getShip(j).getId())
			{

				PhysicsObjectId shipObjId = shipObj.getId();
				PhysicsObjectId shipId = fleets[i].getShip(j).getId();
				if(fleets[i].getShip(j).isAlive() && GeometricCollisions::sphereVsSphere(shipObj.getPosition(), shipObj.getRadius(), 
					fleets[i].getShip(j).getPosition(), fleets[i].getShip(j).getRadius()) && shipObjId.m_fleet != shipId.m_fleet )
				{
						shipObj.markDead (false);
						//ship[j].markDead (false);
						fleets[i].getShip(j).markDead (false);
				}
			}
		}
	}
}


//The collision handling function for a bullet should handle collisions with all planetoids and then with all ships.
void World::collisionHandlingForBullet(Vector3 bulletPosition, int n){

	// handling collision between the player ship and the ring particles
	if(collisionCheck.collisionWithRingParticles(bulletPosition, 0.0)){
		bullet[n].markDead(false);
		return;
	}

	//collision handling function for a bullet should start by handling collisions with planetoids
	for (int i = 0; i < NumberOfMoons; i ++){
		//If anything collides with a planetoid, the planetoid should be unaffected 
		//and the other object should be marked as dying. 
		if (GeometricCollisions::pointVsSphere(bulletPosition, planetoidMoon[i].getPosition(), planetoidMoon[i].getRadius()))
		{
			bullet[n].markDead (false);
		}
	}

	// checking collision with saturn
	if (GeometricCollisions::pointVsSphere(bulletPosition, planetoidSaturn.getPosition(), planetoidSaturn.getRadius()))
	{
		bullet[n].markDead (false);
	}

	//Then it should handle any collisions with ships that are not the source

	for(int i = FIRST_FLEET; i < NUMBER_OF_FLEETS; i++){
		for(int j = 0; j < 13; j++){

			if(bullet[n].getSourceId() != fleets[i].getShip(j).getId()){
				if(GeometricCollisions::pointVsSphere(bulletPosition, fleets[i].getShip(j).getPosition(), fleets[i].getShip(j).getRadius())){
					bullet[n].markDead(false);
					fleets[i].getShip(j).reduceHealth(1);
				}
			}
		}
	}

	// collision handling for bullet and player ship
	if(bullet[n]. getSourceId() != player_ship.getId()){
			if(GeometricCollisions::pointVsSphere(bulletPosition, player_ship.getPosition(), player_ship.getRadius())){
				bullet[n].markDead(false);
			     player_ship.reduceHealth(1);
			}
		}
}


