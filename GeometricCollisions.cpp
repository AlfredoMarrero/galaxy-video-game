#include "GeometricCollisions.h"
#include <math.h>       /* sqrt */
#include "ObjLibrary\Vector3.h"



//
//  pointVsSphere
//
//  Purpose: A function to test whether the specified point is
//           inside the specified sphere.
//  Parameter(s):
//    <1> point: The position of the point
//    <2> sphere_center: The position of the sphere center
//    <3> sphere_radius: The radius of the sphere
//  Precondition(s):
//    <1> sphere_radius >= 0.0
//  Returns: Whether the point is inside the sphere.  If the
//           point is just on the edge of the sphere, false is
//           returned.
//  Side Effect: N/A
//

bool GeometricCollisions::pointVsSphere (const Vector3& point,
										 const Vector3& sphere_center,
										 double sphere_radius)
{


	double distance = sqrt((point.x - sphere_center.x)*(point.x - sphere_center.x) + 
						   (point.y - sphere_center.y)*(point.y - sphere_center.y) +
						   (point.z - sphere_center.z)*(point.z - sphere_center.z));


	// remover el signo de igual si quiero regresar falso cuado solo se tocan
	if(distance <= sphere_radius){
	return true;
	}

	return false;
}

//
//  sphereVsSphere
//
//  Purpose: A function to test whether the two specified
//           spheres intersect.
//  Parameter(s):
//    <1> sphere1_center: The position of the first sphere
//                        center
//    <2> sphere1_radius: The radius of the first sphere
//    <3> sphere2_center: The position of the second sphere
//                        center
//    <4> sphere2_radius: The radius of the second sphere
//  Precondition(s):
//    <1> sphere1_radius >= 0.0
//    <2> sphere2_radius >= 0.0
//  Returns: Whether the first and second spheres intersect.  If
//           the spheres are just touching, false is returned.
//  Side Effect: N/A
//

bool GeometricCollisions::sphereVsSphere (const Vector3& sphere1_center,
										  double sphere1_radius,
										  const Vector3& sphere2_center,
										  double sphere2_radius)
{


		//std::cout<<"from shereVsSphere shipPosition  " <<sphere1_center<< "  shipRadius "<< sphere1_radius<<"  planetoidSaturn.getPosition()  "<<sphere2_center<<" planetoidSaturn.getRadius() " << sphere2_radius<< std::endl;



	double distance = sqrt((sphere1_center.x - sphere2_center.x)*(sphere1_center.x - sphere2_center.x) + 
		                   (sphere1_center.y - sphere2_center.y)*(sphere1_center.y - sphere2_center.y) +
	                   	   (sphere1_center.z - sphere2_center.z)*(sphere1_center.z - sphere2_center.z));

	
	//remover el signo de igual si quiero regresar falso cundo se tocan
	//std::cout<<"distance between objects  "<<distance<<"  radius"<<sphere1_radius + sphere2_radius<< std::endl;
		
	if(distance <= sphere1_radius + sphere2_radius){
	//	std::cout<<"distance between objects  "<<distance<<"  radius"<<sphere1_radius + sphere2_radius<< std::endl;
		//std::cout<<"---------------------------------- 0000000000000000000000000000000000000000000 "<< std::endl;
		return true;
	}

	return false;
}

//
//  pointVsCuboid
//
//  Purpose: A function to test whether the specified point is
//           inside the specified axis-aligned retangular
//           cuboid.  A cuboid is like a cube, but it can have a
//           different side length along each axis.
//  Parameter(s):
//    <1> point: The position of the point
//    <2> cuboid_center: The position of the cuboid center
//    <3> cuboid_size: The distance from the cuboid center to
//                     the faces along each axis;
//                     Half the side length along each axis;
//                     Corresponds to the radius of a sphere
//  Precondition(s):
//    <1> cuboid_size.isAllComponentsNonNegative()
//  Returns: Whether the point is inside the cuboid.  If the
//           point is just on the edge of the cuboid, false is
//           returned.
//  Side Effect: N/A
//

bool GeometricCollisions::pointVsCuboid (const Vector3& point,
										 const Vector3& cuboid_center,
										 const Vector3& cuboid_size)
{

	if(point.x >cuboid_center.x + cuboid_size.x ||cuboid_center.x - cuboid_size.x  >  point.x){
		return false;
	}
	if(point.y > cuboid_center.y + cuboid_size.y || cuboid_center.y - cuboid_size.y > point.y){
		return false;
	}
	if(point.z > cuboid_center.z + cuboid_size.z || cuboid_center.z - cuboid_size.z > point.z){
		return false;
	}

/*
	  return (point.x >= cuboid_center.x - cuboid_size.x  && point.x <= cuboid_center.x + cuboid_size.x) &&
         (point.y >= cuboid_center.y - cuboid_size.y && point.y <= cuboid_center.y + cuboid_size.y) &&
         (point.z >= cuboid_size.z > point.z && point.z <= cuboid_center.z + cuboid_size.z);*/

	return true;

}

//
//  cuboidVsCuboid
//
//  Purpose: A function to test whether the specified
//           axis-aligned retangular cuboids intersect.  A
//           cuboid is like a cube, but it can have a different
//           side length along each axis.
//  Parameter(s):
//    <1> cuboid1_center: The position of the first cuboid
//                        center
//    <2> cuboid1_size: The distance from the first cuboid
//                      center to the faces along each axis;
//                      Half the side length along each axis;
//                      Corresponds to the radius of a sphere
//    <3> cuboid2_center: The position of the second cuboid
//                        center
//    <4> cuboid2_size: The distance from the second cuboid
//                      center to the faces along each axis
//  Precondition(s):
//    <1> cuboid1_size.isAllComponentsNonNegative()
//    <2> cuboid2_size.isAllComponentsNonNegative()
//  Returns: Whether the 2 cuboids intersect.  If the cuboids
//           are just touching, false is returned.
//  Side Effect: N/A
//

	bool GeometricCollisions::cuboidVsCuboid (const Vector3& cuboid1_center,
	                     const Vector3& cuboid1_size,
	                     const Vector3& cuboid2_center,
						 const Vector3& cuboid2_size)
	{
		// remover el signo de igual si quiero regresar falso cuando solo se tocan
		if( cuboid2_center.x - cuboid2_size.x >= cuboid1_center.x + cuboid1_size.x || cuboid1_center.x - cuboid1_size.x >= cuboid2_center.x + cuboid2_size.x){
			return false;
		}
		if(cuboid2_center.y - cuboid2_size.y >= cuboid1_center.y + cuboid1_size.y|| cuboid1_center.y - cuboid1_size.y >= cuboid2_center.y + cuboid2_size.y){
			return false;
		}
		if(cuboid2_center.z - cuboid2_size.z >= cuboid1_center.z + cuboid1_size.z || cuboid1_center.z - cuboid1_size.z >= cuboid2_center.z + cuboid2_size.z){
			return false;
		}

		return true;
	}

//
//  sphereVsCuboid
//
//  Purpose: A function to test whether the specified sphere and
//           cuboid intersect.
//  Parameter(s):
//    <1> sphere_center: The position of the sphere center
//    <2> sphere_radius: The radius of the sphere
//    <3> cuboid_center: The position of the cuboid center
//    <4> cuboid_size: The distance from the cuboid center to
//                     the faces along each axis;
//                     Half the side length along each axis;
//                     Corresponds to the radius of a sphere
//  Precondition(s):
//    <1> sphere_radius >= 0.0
//    <2> cuboid_size.isAllComponentsNonNegative()
//  Returns: Whether the sphere and cuboid intersect.  If they
//           are just touching, false is returned.
//  Side Effect: N/A
//

bool GeometricCollisions::sphereVsCuboid (const Vector3& sphere_center,
	                     double sphere_radius,
	                     const Vector3& cuboid_center,
						 const Vector3& cuboid_size)
{
	double closestX, closestY, closestZ;

	// finding the closest point for x dimension
	if(sphere_center.x <= cuboid_center.x - cuboid_size.x){
		closestX = cuboid_center.x - cuboid_size.x;
	}
	else if(sphere_center.x  >= cuboid_center.x + cuboid_size.x){
		closestX = cuboid_center.x + cuboid_size.x;
	}
	else{
		closestX = sphere_center.x;
	}

	// finding the closest point for y dimension
	if(sphere_center.y <= cuboid_center.y - cuboid_size.y){
		closestY = cuboid_center.y - cuboid_size.y;
	}
	else if(sphere_center.y >= cuboid_center.y + cuboid_size.y){
		closestY = cuboid_center.y + cuboid_size.y;
	}
	else{
		closestY = sphere_center.y;
	}

	//finding the clodest point for z dimension
	if(sphere_center.z <= cuboid_center.z - cuboid_size.z){
		closestZ =  cuboid_center.z - cuboid_size.z;
	}
	else if( sphere_center.z >= cuboid_center.z + cuboid_size.z){
		closestZ = cuboid_center.z + cuboid_size.z;
	}
	else{
		closestZ = sphere_center.z;
	}



	double distance = sqrt((closestX - sphere_center.x) * (closestX - sphere_center.x) +
						(closestY - sphere_center.y) *  (closestY - sphere_center.y) +
						(closestZ - sphere_center.z) *  (closestZ - sphere_center.z));
	// remover el signo de igual si quiero regresar falso cunando se tocan
	return distance <= sphere_radius;
}
