//
//  Marker.cpp
//

#include <cassert>

#include "ObjLibrary/Vector3.h"

#include "WorldInterface.h"
#include "PhysicsObjectId.h"
#include "PhysicsObject.h"
#include "Marker.h"

namespace
{
	const double RADIUS = 0.0;
}



Marker :: Marker ()
		: PhysicsObject(RADIUS)
{
}

Marker :: Marker (const PhysicsObjectId& id,
                  const Vector3& position,
                  const Vector3& velocity)
		: PhysicsObject(RADIUS)
{
	assert(id != PhysicsObjectId::ID_NOTHING);

	setId(id);
	setPosition(position);
	setVelocity(velocity);
}

Marker :: Marker (const Marker& original)
		: PhysicsObject(original)
{
}

Marker :: ~Marker ()
{
}

Marker& Marker :: operator= (const Marker& original)
{
	PhysicsObject::operator=(original);
	return *this;
}



void Marker :: init (const PhysicsObjectId& id,
                     const Vector3& position,
                     const Vector3& velocity)
{
	assert(id != PhysicsObjectId::ID_NOTHING);

	setId(id);
	setPosition(position);
	setVelocity(velocity);
}



///////////////////////////////////////////////////////////////
//
//  Virtual functions inherited from PhysicsObject
//

PhysicsObject* Marker :: getClone () const
{
	return new Marker(*this);
}

bool Marker :: isAlive () const
{
	return true;
}

bool Marker :: isDying () const
{
	assert(isAlive());

	return false;
}

void Marker :: markDead (bool instant)
{
}

void Marker :: update (WorldInterface& r_world)
{
	updateBasic();
}

