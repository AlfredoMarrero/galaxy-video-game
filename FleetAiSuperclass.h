//
//  FleetAiSuperclass.h
//
//  An abstract superclass for classes to provide overarching AI
//    control for a Fleet.
//
//  DO NOT MODIFY THIS FILE
//

#ifndef FLEET_AI_SUPERCLASS_H
#define FLEET_AI_SUPERCLASS_H

#include <cassert>
#include <vector>

#include "ObjLibrary/Vector3.h"

#include "PhysicsObjectId.h"
#include "PhysicsObject.h"
#include "ShipAiInterface.h"
#include "Ship.h"
#include "AiShipReference.h"
#include "Marker.h"

class WorldInterface;



//
//  FleetAiSuperclass.h
//
//  An abstract superclass for classes to provide overarching AI
//    control for a Fleet.  This class provides functionallity
//    to keep track of the Ships and Markers assoviated with the
//    fleet.  When a FleetAiSuperclass is initialized, it stores
//    references to the Ships and Markers it is to control.
//    These Ships and Markers can be accessed by the subclass,
//    and will remain valid as long as the fleet AI exists.
//
//  Subclasses must provide implementations for functions to
//    initialize the AI and update it, and for a number of query
//    functions used to query the AI when initializing the ships
//    the Fleet will control.
//
//  The steps for creating an instance of a FleetAiSuperclass
//    subclass and attaching it to a fleet are as follows:
//    <1> A new instance of the subclass (named p_fas) is
//        created
//    <2> The command ship for the fleet is initialized
//    <3> The fighters for the fleet are initialized
//        sequentially, each using data from the following
//        functions:
//        -> p_fas->getFighterStartPosition
//        -> p_fas->getFighterStartForward
//    <4> The Markers for the fleet are initialized to be
//        stationary and at the origin
//    <5> p_fas->init is called with the ships as parameters.
//        This function should:
//        -> call setShipsAndMarkers
//        -> set the unit AIs for the individual Ships
//    <6> Initialization is complete
//
//  Class Invariant:
//    <1> mv_fighters.size() == FIGHTER_COUNT
//    <2> mvp_markers.size() == MARKER_COUNT
//    <3> m_command_ship.isShip() == mv_fighters[i].isShip()
//                                  WHERE 0 <= i < FIGHTER_COUNT
//    <4> m_command_ship.isShip() == (mvp_markers[i] != NULL)
//                                   WHERE 0 <= i < MARKER_COUNT
//

class FleetAiSuperclass
{
public:
//
//  FIGHTER_COUNT
//
//  The number of fighters in a fleet.
//

	static const unsigned int FIGHTER_COUNT = 12;

//
//  MARKER_COUNT
//
//  The number of markers controlled by a fleet.
//

	static const unsigned int MARKER_COUNT = 10;

public:
//
//  Destructor
//
//  Purpose: To safely destroy an FleetAiSuperclass without
//           memory leaks.  We need a virtual destructor in the
//           base class so that the destructor for the correct
//           derived class will be invoked.
//  Parameter(s): N/A
//  Precondition(s): N/A
//  Returns: N/A
//  Side Effect: All dynamically allocated memory associated
//               with this FleetAiSuperclass is freed.
//

	virtual ~FleetAiSuperclass ()
	{ }  // do nothing

//
//  isInitialized
//
//  Purpose: To determine if this FleetAiSuperclass has been
//           initialized.  This function is inlined for speed
//           reasons.
//  Parameter(s): N/A
//  Precondition(s): N/A
//  Returns: Whether this FleetAiSuperclass is initialized.
//  Side Effect: N/A
//

	bool isInitialized () const
	{	return m_command_ship.isShip();	}

//
//  getCloneUninitialized
//
//  Purpose: To create a dynamically allocated copy of this
//           uninitialized FleetAiSuperclass.
//  Parameter(s): N/A
//  Precondition(s):
//    <1> !isInitialized()
//  Returns: A deep copy of this FleetAiSuperclass is created.
//  Side Effect: N/A
//

	virtual FleetAiSuperclass* getCloneUninitialized (
	                                                ) const = 0;

//
//  getCloneInitialized
//
//  Purpose: To create a dynamically allocated near-copy of this
//           initialized FleetAiSuperclass.  This near-copy
//           should be a deep copy of the current
//           FleetAiSuperclass, except that it should control
//           the specified Ships and Markers.
//  Parameter(s):
//    <1> command_ship: The command ship to control
//    <2> v_fighters: An STL vector of the fighters to control
//    <3> vp_markers: An STL vector of pointers to the Markers
//                    to control
//  Precondition(s):
//    <1> isInitialized()
//    <2> command_ship.isShip()
//    <3> v_fighters.size() == FIGHTER_COUNT
//    <4> v_fighters[i].isShip()
//                                  WHERE 0 <= i < FIGHTER_COUNT
//    <5> vp_markers.size() == MARKER_COUNT
//    <6> vp_markers[i] != NULL
//                                   WHERE 0 <= i < MARKER_COUNT
//  Returns: A deep near-copy of this FleetAiSuperclass that
//           controls Ships command_ship, v_fighters, and
//           Markers v_markers is created.  This near-copy
//           should be created using a modified copy
//           constructor for the derived class.
//  Side Effect: N/A
//

	virtual FleetAiSuperclass* getCloneInitialized (
	                AiShipReference command_ship,
	                std::vector<AiShipReference>& v_fighters,
	                std::vector<Marker*>& vp_markers) const = 0;

//
//  getFighterStartPosition
//
//  Purpose: To retrieve the starting position for the specified
//           fighter.  Note that this function will only be
//           called before this FleetAiSuperclass is initialized.
//  Parameter(s):
//    <1> index: Which fighter
//  Precondition(s):
//    <1> index < FIGHTER_COUNT
//  Returns: The starting position for fighter index in the
//           local coordinate system for the command ship.  The
//           local ccordinate system is represented as if the
//           command ship was at the origin and facing along the
//           +X-axis, with the +Y-axis as an up vector.
//  Side Effect: N/A
//

	virtual Vector3 getFighterStartPosition (
	                              unsigned int index) const = 0;

//
//  getFighterStartForward
//
//  Purpose: To retrieve the starting forward direction for the
//           specified fighter.  The fighter will start moving
//           in this direction at its maximum speed.  Note that
//           this function will only be called before this
//           FleetAiSuperclass is initialized.
//  Parameter(s):
//    <1> index: Which fighter
//  Precondition(s):
//    <1> index < FIGHTER_COUNT
//  Returns: The starting forward direction for fighter index in
//           the local coordinate system for the command ship.
//           The local ccordinate system is represented as if
//           the command ship was at the origin and facing along
//           the +X-axis, with the +Y-axis as an up vector.  The
//           vector returned is always a normal vector.
//  Side Effect: N/A
//

	virtual Vector3 getFighterStartForward (
	                              unsigned int index) const = 0;

//
//  draw
//
//  Purpose: To display the current state of this
//           FleetAiSuperclass.  This function is intended for
//           use in debugging and can do nothing.
//  Parameter(s): N/A
//  Precondition(s):
//    <1> isInitialized()
//  Returns: N/A
//  Side Effect: Visual information for this FleetAiSuperclass is
//               displayed.  The default implementation for this
//               function does nothing.
//

	virtual void draw () const;

//
//  init
//
//  Purpose: To initialize this FleetAiSuperclass to control the
//           specified fleet composed of the specified Ships and
//           controlling the specified Markers, and to assign
//           unit AIs to the specified Ships.
//  Parameter(s):
//    <1> world: The World that the fleet is in
//    <2> fleet: The identifier for the fleet this
//               FleetAiSuperclass is to control
//    <3> command_ship: The command ship to control
//    <4> v_fighters: An STL vector of the fighters to control
//    <5> vp_markers: An STL vector of pointers to the Markers
//                    to control
//  Precondition(s):
//    <1> !isInitialized()
//    <2> fleet <= PhysicsObjectId::FLEET_MAX
//    <3> command_ship.isShip()
//    <4> v_fighters.size() == FIGHTER_COUNT
//    <5> v_fighters[i].isShip()
//                                  WHERE 0 <= i < FIGHTER_COUNT
//    <6> vp_markers.size() == MARKER_COUNT
//    <7> vp_markers[i] != NULL
//                                   WHERE 0 <= i < MARKER_COUNT
//  Returns: N/A
//  Side Effect: This FleetAiSuperclass is initialized to control
//               fleet fleet, with command ship command_ship,
//               the the fighters in v_fighters, and the Markers
//               in vp_markers.  A unit AI is created for and
//               assigned to the command ship and each of the
//               fighters.
//

	virtual void init (
	                   const WorldInterface& world,
	                   unsigned int fleet,
	                   AiShipReference command_ship,
	                   std::vector<AiShipReference>& v_fighters,
	                   std::vector<Marker*>& vp_markers) = 0;

//
//  run
//
//  Purpose: To run this FleetAiSuperclass for one frame.  This
//           function is responsible for running the ship AIs
//           for the Ships in its fleet.
//  Parameter(s):
//    <1> world: The World that the fleet is in
//  Precondition(s):
//    <1> isInitialized()
//  Returns: N/A
//  Side Effect: The AI for the controlled fleet is run for one
//               frame.
//

	virtual void run (const WorldInterface& world) = 0;

//
//  updateForPause
//
//  Purpose: To update any internal time values for this
//           FleetAiSuperclass after the game has been paused.
//           This function should be called once each time the
//           game is paused.  Calling this function at other
//           times may result in unexpected behaviour.
//  Parameter(s): N/A
//  Precondition(s): N/A
//  Returns: N/A
//  Side Effect: This FleetAiSuperclass is updated for the most
//               recent pause.  Unit AIs will be updated by the
//               Ships that they control, and so are not updated
//               here.  The default implementation for this
//               function does nothing.
//

	virtual void updateForPause ();

protected:
//
//  Default Constructor
//
//  Purpose: To create a FleetAiSuperclass.
//  Parameter(s): N/A
//  Precondition(s): N/A
//  Returns: N/A
//  Side Effect: A new FleetAiSuperclass is created.  The Ships
//               and Markers to control are not set.
//

	FleetAiSuperclass ();

//
//  Modified Copy Constructor
//
//  Purpose: To create a new FleetAiSuperclass as a copy of
//           another uninititialzed FleetAiSuperclass.  If the
//           other FleetAiSuperclass is initialized, use the
//           other modified copy constructor instead.
//  Parameter(s): N/A
//  Precondition(s):
//    <1> !original.isInitialized()
//  Returns: N/A
//  Side Effect: A new FleetAiSuperclass is created as a deep
//               copy of original.
//

	FleetAiSuperclass (const FleetAiSuperclass& original);

//
//  Modified Copy Constructor
//
//  Purpose: To create a new FleetAiSuperclass that controls the
//           specified ships as a copy of another inititialzed
//           FleetAiSuperclass.  If the other FleetAiSuperclass
//           is not initialized, use the other modified copy
//           constructor instead.
//  Parameter(s):
//    <1> original: The FleetAiSuperclass to copy
//    <2> command_ship: The command ship to control
//    <3> v_fighters: An STL vector of the fighters to control
//    <4> vp_markers: An STL vector of pointers to the Markers
//                    to control
//  Precondition(s):
//    <1> original.isInitialized()
//    <2> command_ship.isShip()
//    <3> v_fighters.size() == FIGHTER_COUNT
//    <4> v_fighters[i].isShip()
//                              WHERE 0 <= i < FIGHTER_COUNT
//    <5> vp_markers.size() == MARKER_COUNT
//    <6> vp_markers[i] != NULL
//                               WHERE 0 <= i < MARKER_COUNT
//  Returns: N/A
//  Side Effect: A new FleetAiSuperclass is created to control
//               Ships command_ship and v_fighters and Markers
//               vp_markers.  In all other ways, the new
//               FleetAiSuperclass is a deep copy of original.
//

	FleetAiSuperclass (const FleetAiSuperclass& original,
	                   AiShipReference command_ship,
	                   std::vector<AiShipReference>& v_fighters,
	                   std::vector<Marker*>& vp_markers);

//
//  Assignment Operator
//
//  This function has intentionally not been implemented.
//

	FleetAiSuperclass& operator= (
	                         const FleetAiSuperclass& original);

//
//  getFleetIndex
//
//  Purpose: To fleet index of the controlled fleet.  The fleet
//           index is the m_fleet component of the ids for the
//           ships in the fleet.  This function is inlined for
//           speed reasons.
//  Parameter(s): N/A
//  Precondition(s):
//    <1> isInitialized()
//  Returns: The fleet index of the controlled fleet.
//  Side Effect: N/A
//

	unsigned int getFleetIndex () const
	{
		assert(isInitialized());

		return m_command_ship.getId().m_fleet;
	}

//
//  getCommandShip
//
//  Purpose: To retrieve a constant reference to the command
//           ship for the controlled fleet.  This function is
//           inlined for speed reasons.
//  Parameter(s): N/A
//  Precondition(s):
//    <1> isInitialized()
//  Returns: A constant reference to the command ship.
//  Side Effect: N/A
//

	const Ship& getCommandShip () const
	{
		assert(isInitialized());

		return m_command_ship.getShip();
	}

//
//  getCommandShipAi
//
//  Purpose: To retrieve a reference to the ShipAiInterface of
//           the command ship for the controlled fleet.  This
//           function is inlined for speed reasons.
//  Parameter(s): N/A
//  Precondition(s):
//    <1> isInitialized()
//  Returns: A non-const reference to the command ship.
//  Side Effect: N/A
//

	ShipAiInterface& getCommandShipAi () const
	{
		assert(isInitialized());

		return m_command_ship.getAi();
	}

//
//  getCommandShipId
//
//  Purpose: To determine the id of the command ship for the
//           controlled fleet.  This function is inlined for
//           speed reasons.
//  Parameter(s): N/A
//  Precondition(s):
//    <1> isInitialized()
//  Returns: The id of the command ship.
//  Side Effect: N/A
//

	PhysicsObjectId getCommandShipId () const
	{
		assert(isInitialized());

		return m_command_ship.getId();
	}

//
//  getCommandShipAiShipReference
//
//  Purpose: To retreive an AiShipReference for the command Ship
//           for the controlled fleet.  The AiShipRefernce will
//           be needed to initialize a unit AI for the command
//           ship.  This function is inlined for speed reasons.
//  Parameter(s): N/A
//  Precondition(s): N/A
//  Returns: An AiShipReference for the command ship.
//  Side Effect: N/A
//

	const AiShipReference& getCommandShipAiShipReference (
	                                                     ) const
	{
		assert(isInitialized());

		return m_command_ship;
	}

//
//  getFighter
//
//  Purpose: To retrieve a constant reference to the specified
//           fighter in the controlled fleet.  This function is
//           inlined for speed reasons.
//  Parameter(s):
//    <1> fighter: Which fighter
//  Precondition(s):
//    <1> isInitialized()
//    <2> fighter < FIGHTER_COUNT
//  Returns: A constant reference to fighter fighter.
//  Side Effect: N/A
//

	const Ship& getFighter (unsigned int fighter) const
	{
		assert(isInitialized());
		assert(fighter < FIGHTER_COUNT);

		assert(mv_fighters.size() == FIGHTER_COUNT);
		return mv_fighters[fighter].getShip();
	}

//
//  getFighterAi
//
//  Purpose: To retrieve a reference to the ShipAiInterface of
//           the specified fighter in the controlled fleet.
//           This function is inlined for speed reasons.
//  Parameter(s):
//    <1> fighter: Which fighter
//  Precondition(s):
//    <1> isInitialized()
//    <2> fighter < FIGHTER_COUNT
//  Returns: A non-const reference to fighter fighter.
//  Side Effect: N/A
//

	ShipAiInterface& getFighterAi (unsigned int fighter) const
	{
		assert(isInitialized());
		assert(fighter < FIGHTER_COUNT);

		assert(mv_fighters.size() == FIGHTER_COUNT);
		return mv_fighters[fighter].getAi();
	}

//
//  getFighterId
//
//  Purpose: To determine the id of the specified fighter in the
//           controlled fleet.  This function is inlined for
//           speed reasons.
//  Parameter(s):
//    <1> fighter: Which fighter
//  Precondition(s):
//    <1> isInitialized()
//    <2> fighter < FIGHTER_COUNT
//  Returns: The id of fighter fighter.
//  Side Effect: N/A
//

	PhysicsObjectId getFighterId (unsigned int fighter) const
	{
		assert(isInitialized());
		assert(fighter < FIGHTER_COUNT);

		assert(mv_fighters.size() == FIGHTER_COUNT);
		return mv_fighters[fighter].getId();
	}

//
//  getFighterAiShipReference
//
//  Purpose: To retreive an AiShipReference for the specified
//           fighter in the controlled fleet.  The
//           AiShipRefernce will be needed to initialize a unit
//           AI for the command ship.  This function is inlined
//           for speed reasons.
//  Parameter(s):
//    <1> fighter: Which fighter
//  Precondition(s):
//    <1> isInitialized()
//    <2> fighter < FIGHTER_COUNT
//  Returns: An AiShipReference for fighter fighter.
//  Side Effect: N/A
//

	const AiShipReference& getFighterAiShipReference (
	                                 unsigned int fighter) const
	{
		assert(isInitialized());
		assert(fighter < FIGHTER_COUNT);

		return mv_fighters[fighter];
	}

//
//  getMarker
//
//  Purpose: To retrieve a reference to the specified Marker
//           associated with the controlled fleet.  This
//           function is inlined for speed reasons.
//  Parameter(s):
//    <1> marker: Which Marker
//  Precondition(s):
//    <1> isInitialized()
//    <2> marker < MARKER_COUNT
//  Returns: A reference to Marker marker.
//  Side Effect: N/A
//

	Marker& getMarker (unsigned int marker) const
	{
		assert(isInitialized());
		assert(marker < MARKER_COUNT);

		assert(mvp_markers.size() == MARKER_COUNT);
		assert(mvp_markers[marker] != NULL);
		return *(mvp_markers[marker]);
	}

//
//  getMarkerId
//
//  Purpose: To determine the id of the specified Marker
//           associated with the controlled fleet.  This
//           function is inlined for speed reasons.
//  Parameter(s):
//    <1> marker: Which Marker
//  Precondition(s):
//    <1> isInitialized()
//    <2> marker < MARKER_COUNT
//  Returns: The id of Marker marker.
//  Side Effect: N/A
//

	PhysicsObjectId getMarkerId (unsigned int marker) const
	{
		assert(isInitialized());
		assert(marker < MARKER_COUNT);

		assert(mvp_markers.size() == MARKER_COUNT);
		assert(mvp_markers[marker] != NULL);
		return (mvp_markers[marker])->getId();
	}

//
//  setShipsAndMarkers
//
//  Purpose: To set the Ships and Markers that will be
//           controlled by this FleetAiSuperclass.
//  Parameter(s):
//    <1> command_ship: The command ship to control
//    <2> v_fighters: An STL vector of the fighters to control
//    <3> vp_markers: An STL vector of pointers to the Markers
//                    to control
//  Precondition(s):
//    <1> command_ship.isShip()
//    <2> v_fighters.size() == FIGHTER_COUNT
//    <3> v_fighters[i].isShip()
//                                  WHERE 0 <= i < FIGHTER_COUNT
//    <4> vp_markers.size() == MARKER_COUNT
//    <5> vp_markers[i] != NULL
//                                   WHERE 0 <= i < MARKER_COUNT
//  Returns: N/A
//  Side Effect: This FleetAiSuperclass is set to control
//               command ship command_ship, the the fighters in
//               v_fighters, and the Markers in vp_markers.
//

	void setShipsAndMarkers (
	                   AiShipReference command_ship,
	                   std::vector<AiShipReference>& v_fighters,
	                   std::vector<Marker*>& vp_markers);

private:
//
//  invariant
//
//  Purpose: To determine if the class invariant is true.
//  Parameter(s): N/A
//  Precondition(s): N/A
//  Returns: Whether the class invariant is true.
//  Side Effect: N/A
//

	bool invariant () const;

private:
	AiShipReference m_command_ship;
	std::vector<AiShipReference> mv_fighters;
	std::vector<Marker*> mvp_markers;
};



#endif
