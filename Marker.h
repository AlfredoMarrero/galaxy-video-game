//
//  Marker.h
//

#ifndef MARKER_H
#define MARKER_H

#include "ObjLibrary/Vector3.h"

#include "PhysicsObject.h"

class WorldInterface;
struct PhysicsObjectId;



//
//  Marker
//
//  A class to represent a marker in space.  Markers are not
//    displayed and do not generate collisions.  They do,
//    however, move according to their velocity and can be
//    queried as PhysicsObjects.
//

class Marker : public PhysicsObject
{
public:
//
//  Default Constructor
//
//  Purpose: To create a new Marker at the origin.
//  Paremeter(s): N/A
//  Precondition(s): N/A
//  Returns: N/A
//  Side Effect: A new Marker is created at the origin with
//               an id of ID_DEFAULT and a radius of
//               RADIUS_DEFAULT.  No display list is set.
//

	Marker ();

//
//  Constructor
//
//  Purpose: To create a new Marker with the specified id,
//           position, and radius.
//  Paremeter(s):
//    <1> id: The id
//    <2> position: The starting position
//    <3> velocity: The starting velocity
//  Precondition(s)
//    <1> id != PhysicsObjectId::ID_NOTHING
//  Returns: N/A
//  Side Effect: A new Marker is created at position position
//               with an id of id and a velocity of velocity.  If
//               velocity is non-zero, the new Marker is facing
//               in the direction it is moving.  The up vector
//               is undefined but guarenteed to be a right angle
//               to the forward vector.  If velocity is zero,
//               the new Marker is facing along the positive
//               X-axis with +Y as up.  No display list is set.
//

	Marker (const PhysicsObjectId& id,
	        const Vector3& position,
	        const Vector3& velocity);

//
//  Copy Constructor
//
//  Purpose: To create a new Marker as a copy of another.
//  Paremeter(s):
//    <1> original: The Marker to copy
//  Precondition(s): N/A
//  Returns: N/A
//  Side Effect: A new Marker is created with the same id,
//               position, and velocity as original.
//

	Marker (const Marker& original);

//
//  Destructor
//
//  Purpose: To safely destroy a Marker without memory leaks.
//  Paremeter(s): N/A
//  Precondition(s): N/A
//  Returns: N/A
//  Side Effect: All dynamically allocated memory associated
//               with this Marker is freed.
//

	virtual ~Marker ();

//
//  Assignment Operator
//
//  Purpose: To modify this Marker to be a copy of another.
//  Paremeter(s):
//    <1> original: The Marker to copy
//  Precondition(s): N/A
//  Returns: A reference to this Marker.
//  Side Effect: The Marker is set to have the same id,
//               position, and velocity.
//

	Marker& operator= (const Marker& original);

//
//  init
//
//  Purpose: To initialize this Marker to be at the specified
//           position with the specified velocity.
//  Parameter(s):
//    <1> id: The id
//    <2> position: The starting position
//    <3> velocity: The starting velocity
//  Precondition(s):
//    <1> id != PhysicsObjectId::ID_NOTHING
//  Returns: N/A
//  Side Effect: This Marker is set to be at position position
//               with an id of id a velocity of velocity.  If
//               velocity is non-zero, this Marker is facing in
//               the direction it is moving.  The up vector is
//               undefined but guarenteed to be a right angle to
//               the forward vector.  If velocity is zero,
//               this Marker is facing along the positive X-axis
//               with +Y as up.  This Marker is set to not have
//               a display list.
//

	void init (const PhysicsObjectId& id,
	           const Vector3& position,
	           const Vector3& velocity);

///////////////////////////////////////////////////////////////
//
//  Virtual functions inherited from PhysicsObject
//

//
//  getClone
//
//  Purpose: To create a dynamically allocated copy of this
//           PhysicsObject.
//  Parameter(s): N/A
//  Precondition(s): N/A
//  Returns: A deep copy of this PhysicsObject.  This copy
//           should be created using the copy constructor for
//           the derived class.
//  Side Effect: N/A
//

	virtual PhysicsObject* getClone () const;

//
//  isAlive
//
//  Purpose: To determine if this PhysicsObject is alive.
//  Parameter(s): N/A
//  Precondition(s): N/A
//  Returns: Whether this PhysicsObject is alive.  If this
//           PhysicsObject is of a type that cannot become dead,
//           true is returned.
//  Side Effect: N/A
//

	virtual bool isAlive () const;

//
//  isDying
//
//  Purpose: To determine if this PhysicsObject is currently
//           dying.  This function can be used to prevent a
//           PhysicsObject being counted twice.  For example, a
//           bullet should not hit the same target twice.
//  Parameter(s): N/A
//  Precondition(s):
//    <1> isAlive()
//  Returns: Whether this PhysicsObject is going to be marked as
//           dead the next time that update is called.  If this
//           PhysicsObject is of a type that cannot become dead,
//           false is returned.
//  Side Effect: N/A
//

	virtual bool isDying () const;

//
//  markDead
//
//  Purpose: To mark this PhysicsObject as dead.  This function
//           does nothing.
//  Parameter(s):
//    <1> instant: Whether the PhysicsObject should be marked as
//                 dead immediately
//  Precondition(s): N/A
//  Returns: N/A
//  Side Effect: If this PhysicsObject is of a type that cannot
//               become dead, there is no effect. Otherwise, if
//               instant == true, this PhysicisObject is
//               marked as dead and no death actions - such as
//               generating a death explosion - are performed.
//               If this PhysicsObject can become dead and
//               instant == false, this PhysicsObject is marked
//               as dying.  The next time that update is called,
//               any death actions - such as generating a death
//               explosion - will be performed and this
//               PhysicsObject will be marked as dead.
//

	virtual void markDead (bool instant);

//
//  update
//
//  Purpose: To update this PhysicsObject for one frame.  This
//           function does not perform collision checking or
//           handling.
//  Parameter(s):
//    <1> r_world: An interface to the world this PhysicsObject
//                 is in
//  Precondition(s): N/A
//  Returns: N/A
//  Side Effect: This PhysicsObject is updated for one frame.
//               Any queries about or changes to the world it is
//               in are resolved through r_world.
//

	virtual void update (WorldInterface& r_world);

};



#endif
