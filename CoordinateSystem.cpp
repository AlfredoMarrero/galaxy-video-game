

// 
// ALfredo Marrero 
// 200 330 199
// implementation of class CoordinateSystem
// Coordinate system.cpp

#include "CoordinateSystem.h"

// constructor
CoordinateSystem::CoordinateSystem()
{
	forward.set( 0,  0, -1);
	up.set( 0,  1,  0);
	right = forward.crossProduct(up);

}


// constructor
CoordinateSystem::CoordinateSystem(Vector3 positionV,Vector3 forwardV, Vector3 upV){
	position = positionV;
	forward = forwardV;
	up = upV;
	right = forward.crossProduct(up);
}

void CoordinateSystem::applyTransformation() const
{
  // code for translation will go here
	 glTranslated(position.x, position.y, position.z);
	// Vector3 right = getRight();
  // code for rotation will go here

    double a_matrix[16] =
   { right.x,    right.y,    right.z,   0.0,
     forward.x,  forward.y,  forward.z, 0.0,
     up.x,       up.y,       up.z,      0.0,
     0.0,        0.0,        0.0,       1.0,  };

	 glMultMatrixd(a_matrix);
	 //tr3
	// teapot.rotateAroundForward(-1.0);
}





// setting the initial position of the camera
void CoordinateSystem::setPosition(){
	position.set(70000.0, 0.0, 70000.0);
}
// move the camera forward
void CoordinateSystem::moveForward(){
	position += forward * 50;
}

// move forward with F key at a faster speed
void CoordinateSystem::moveForwardSpeed(){
	position += forward * 5000;
}
// moves the camera backward
void CoordinateSystem::moveBackwards(){
	position -= forward * 50;

}
// moves the camera left
void CoordinateSystem::moveLeft(){
	position -= right * 50;
}
// moves the camera right
void CoordinateSystem::moveRight(){

	position += right * 5000;
}
// moves the camera up
void CoordinateSystem::moveUp(){
	position += up * 5000;

}
// moves the camera down
void CoordinateSystem::moveDown(){
	position -= up * 5000;

}

// this function rotates the camera towards the planet 
void CoordinateSystem::rotateCameraToVector()
{
	Vector3 origin = Vector3::ZERO;
	Vector3 target_facing;
	double max_radians = 0.05;
	target_facing = origin - position;

	if(target_facing.isZero())
		return;

	Vector3 axis = forward.crossProduct(target_facing);

	if(axis.isZero()){
		axis = up;
	}
	else{
		axis.normalize();
	}

	double radians = forward.getAngleSafe(target_facing);

	if(radians > max_radians){
		radians = max_radians;
	}

	forward.rotateArbitrary(axis, radians);
	up.rotateArbitrary(axis, radians);
	right.rotateArbitrary(axis, radians);
}

void CoordinateSystem::rotateRight(){
	forward.rotateArbitrary(up, -0.05);
	right.rotateArbitrary(up, -0.05);
}

void CoordinateSystem::rotateLeft(){
	forward.rotateArbitrary(up, 0.05);
	right.rotateArbitrary(up, 0.05);
}

void CoordinateSystem::rotateUp(){
	forward.rotateArbitrary(right, 0.05);
	up.rotateArbitrary(right, 0.05);
}

void CoordinateSystem::rotateDown(){
	forward.rotateArbitrary(right, -0.05);
	up.rotateArbitrary(right, -0.05);
}


Vector3 CoordinateSystem::getPosition(){
	return position;
}

Vector3 CoordinateSystem::getUp(){
	return up;
}


Vector3 CoordinateSystem::getForward(){
	return forward;
}


Vector3 CoordinateSystem::getUpVector(const Vector3& local_forward){

	return calculateUpVector(local_forward);
}



Vector3 CoordinateSystem::calculateUpVector(const Vector3& local_forward) const
{
	// code will go here
	static const Vector3 IDEAL_UP_VECTOR(0.0, 1.0, 0.0);
	static const double HALF_PI = 1.5707963267948966;

	if(local_forward.isZero()){
		return IDEAL_UP_VECTOR;
	}

	Vector3 axis = local_forward.crossProduct(IDEAL_UP_VECTOR);
	if(axis.isZero()){
		return Vector3(1.0, 0.0, 0.0);
	}
	else
	{
		axis.normalize();
		// more code here
	}

	 Vector3 local_up = local_forward.getRotatedArbitrary(axis, HALF_PI);
     return local_up;

}



void CoordinateSystem::setOrientation(Vector3 forwardVector){

	//forward = calculateUpVector(forwardVector);
	up = calculateUpVector(forwardVector);
	//up = calculateUpVector(forward);
    forward= calculateUpVector(up);
	//forward = upVector;
	//up = upVector;

}



void CoordinateSystem::rotateUpright(double max_radians)
{
  Vector3 desired_up = calculateUpVector(forward);
  Vector3 axis = up.crossProduct(desired_up);
  if(axis.isZero()){
	  axis = forward;
  }
  else{
	  axis.normalize();
  }


  double radians = up.getAngleSafe(desired_up);
  if(radians > max_radians){
       radians = max_radians;
  }


 up.rotateArbitrary(axis, radians);

}


Vector3 CoordinateSystem::getPosition(Vector3 moon_position, double moon_radius){
	Vector3 positionPhyO;

	positionPhyO = moon_position + Vector3::getRandomUnitVector() * (moon_radius + 500.0);
	return positionPhyO;
}