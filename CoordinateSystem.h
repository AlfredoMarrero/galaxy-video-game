
// ALfredo Marrero
// 200 330 199
 //interface for class CoordinateSystem
// CoordinateSystem.h
#include "ObjLibrary/Vector3.h"
#include "GetGlut.h"
using namespace std;

 //preventing multiple declarations
#ifndef COORDINATE_SYSTEM_H
#define COORDINATE_SYSTEM_H


 //class for the camera
class CoordinateSystem{

public:
	 //construtor
	CoordinateSystem();
	CoordinateSystem(Vector3 positionV,Vector3 forwardV, Vector3 upV);
	// functions
	void setPosition();
	void moveForward();
	void moveBackwards();
	void moveLeft();
	void moveRight();
	void moveUp();
	void moveDown();
	void rotateCameraToVector();
	void rotateRight();
	void rotateLeft();
	void rotateUp();
	void rotateDown();
	void moveForwardSpeed();
	void setOrientation(Vector3 forwardVector);
	void rotateUpright(double max_radians);
	void applyTransformation() const;
	Vector3 getPosition(Vector3 moon_position, double moon_radius);

	Vector3 getUpVector(const Vector3& local_forward);

	//getters
	Vector3 getPosition();
	Vector3 getForward();
	Vector3 getUp();

private:

	//member variables
	Vector3 forward;
	Vector3 up;
	Vector3 right;
	Vector3 position;

	Vector3 calculateUpVector(const Vector3& local_forward) const;
};

#endif