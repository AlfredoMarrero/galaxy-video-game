//
// ALfredo Marrero
// 200 330 199
// interface for class CoordinateSystem
// DrawRingParticles.cpp


#include "DrawRingParticles.h"
#include "ObjLibrary/ObjModel.h"

// constructor
DrawRingParticles::DrawRingParticles(){
	particle.radius = 10;
	size = 500;
	counter = 0;

}

// gets the sector ID
sectorID DrawRingParticles::getSectorID(Vector3 position){

	sectorID sector_ID;
	sector_ID.x = floor(position.x/size);
	sector_ID.y = floor(position.y/size);
	sector_ID.z = floor(position.z/size);

	return sector_ID;

}

// gets the position of the center of a sector
Vector3 DrawRingParticles::getPosition_CenterOfSector(sectorID sector_ID){

	Vector3 sectorCenter;
	sectorCenter.x = size * sector_ID.x + size/2;
	sectorCenter.y = size * sector_ID.y + size/2;
	sectorCenter.z = size * sector_ID.z + size/2;

	return sectorCenter;
}



//  Make a function to create or initialize a single sector, given a sector id
void DrawRingParticles::initializeSector(sectorID sector_ID,  DisplayList particlesList){

	Vector3 centerPosition = getPosition_CenterOfSector(sector_ID);

	Vector3  origin;
	//centerPosition.x = centerOfSector.x;
	//centerPosition.y = centerOfSector.y;
	//centerPosition.z = centerOfSector.z;

	int ringDens =  ringDensity.curvedDensityFromMinRawDesity(centerPosition);

	ringDens *= 2;
	if(ringDens>12){
		ringDens =12;
	}
	origin.x = sector_ID.x;
	origin.y = sector_ID.y;
	origin.z = sector_ID.z;

	generateWorleyPoints(getSectorOrigin(origin), ringDens);

	int d= 0;
	for(int i = 0; i < ringDens; i++){
		glPushMatrix();
		glTranslated(particles[i].x, particles[i].y, particles[i].z);
		glScaled(particles[i].radius, particles[i].radius, particles[i].radius);
		particlesList.draw();
		glPopMatrix();
	}
}


// get the origin ofa sector
Vector3 DrawRingParticles::getSectorOrigin(Vector3 sector_ID){

	Vector3 sectorOrigin;

	sectorOrigin.x = sector_ID.x * size;
	sectorOrigin.y = sector_ID.y * size;
	sectorOrigin.z = sector_ID.z * size;
	return sectorOrigin;
}

//function to generate Worley points for a sector in 3D space
void DrawRingParticles::generateWorleyPoints(Vector3 point, int m){

	double r, fraction;

	r = double(pseudorandomValue(point));

	//	cout<<"value of r "<<r << endl;

	for (int i = 0; i < m; i++){
		r= xorshift (unsigned int(r));
		fraction = r/ double(UINT_MAX);
		//	cout <<"fraction "<<fraction<< endl;
		particles[i].x = point.x + fraction * 500; 
		r = xorshift(unsigned int(r));
		fraction = r/double(UINT_MAX);
		particles[i].y = point.y + fraction * 500;
		r = xorshift(unsigned int(r));
		fraction = r/double(UINT_MAX);
		particles[i].z = point.z + fraction * 500;
		particles[i].radius = fraction*fraction * 90 + 10;
		//cout<<"particle value "<<particles[i].radius<< endl;
	}
}



// function to calculate a pseudorandom value for a grid point (specified by three integers)
unsigned int DrawRingParticles::pseudorandomValue(Vector3 values){

	int ix= values.x,
		iy = values.y,
		iz = values.z;

	unsigned int SEED_IX1 = 11200;
	unsigned int SEED_IY1 = 204059;
	unsigned int SEED_IZ1 = 854435;
	unsigned int SEED_I1 = 31244;
	unsigned int SEED_I2 = 593580;
	unsigned int SEED_I0 = 233234;
	unsigned int SEED_IX2 = 532645;
	unsigned int SEED_IY2 = 77743;
	unsigned int SEED_IZ2 = 84859;

	unsigned int n = (SEED_IX1 * ix) ^ (SEED_IY1 * iy) ^ (SEED_IZ1 * iz);
	unsigned int quad_term = SEED_I2 * n * n ^
		SEED_I1 * n ^
		SEED_I0;
	unsigned int value = quad_term ^
		(SEED_IX2 * ix) ^
		(SEED_IY2 * iy) ^
		(SEED_IZ2 * iz);
	return value;
}

//
//  xorshift
//
//  Purpose: To calculate the next pseudorandom value from
//           the specified seed value.
//  Parameter(s):
//    <1> a: The seed value
//  Precondition(s): N/A
//  Returns: The next pseudorandom value after a.
//  Side Effect: N/A
//
//  http://www.jstatsoft.org/v08/i14/paper
//

unsigned int DrawRingParticles::xorshift (unsigned int a)
{

	a ^= a << 13;
	a ^= a >> 17;
	a ^= a << 5;

	return a;
}

// this function displays the sectors by calling initializeSector() with a 3 for loop
void DrawRingParticles::drowCameraNewarBySectors(Vector3 cameraPosition, DisplayList particlesList){

	sectorID sector_ID = getSectorID(cameraPosition);

	sectorID sector_id;
	for( int x = sector_ID.x-2; x <= sector_ID.x + 2; x++){
		for(int y = sector_ID.y-2; y <= sector_ID.y + 2; y++){
			for (int z = sector_ID.z - 2; z <= sector_ID.z + 2; z++){
				sector_id.x = x;
				sector_id.y = y;
				sector_id.z = z;
				initializeSector(sector_id, particlesList);
			}
		}
	}

}


//Add a function to your ring system to detect whether a sphere collides with any ring particles.
//The function should take the sphere center and radius as parameters. 
bool DrawRingParticles::collisionWithRingParticles(Vector3 centerObjPosition, double radiusObj){

	//radius of an expanded sphere, equal to that of the provided sphere plus the maximum radius of a ring particle.
	// This expanded sphere will be used to determine which sectors to check for collisions with.
	double radiusOfExpandedSphere = radiusObj + 100;

	//Determine the minimum and maximum sectors that overlap the expanded sphere along each axis.  
	//Use a triple loop to check each sector in these ranges.
	sectorID sector_ID = getSectorID(centerObjPosition);
	sectorID sector_id;
	for( int x = sector_ID.x-1; x <= sector_ID.x + 1; x++){
		for(int y = sector_ID.y-1; y <= sector_ID.y + 1; y++){
			for (int z = sector_ID.z - 1; z <= sector_ID.z + 1; z++){
				sector_id.x = x;
				sector_id.y = y;
				sector_id.z = z;
				Vector3 positionCenterOfSector= getPosition_CenterOfSector(sector_id);

             	if(GeometricCollisions::sphereVsCuboid(centerObjPosition, 
					radiusOfExpandedSphere, positionCenterOfSector, Vector3(size, size, size)))
				{

					Vector3  origin;
		
					int ringDens =  ringDensity.curvedDensityFromMinRawDesity(positionCenterOfSector);

					ringDens *= 2;
					if(ringDens>12){
						ringDens =12;
					}
					origin.x = sector_ID.x;
					origin.y = sector_ID.y;
					origin.z = sector_ID.z;

					generateWorleyPoints(getSectorOrigin(origin), ringDens);

				//Then check for a collision between each ring particle and the original sphere provided as a parameter. 
				//If there is a collision, immediately return true.  If not, do nothing.
					for(int i = 0; i < ringDens; i++){

						if(radiusObj == 0.0){ // detects collision with bullets
							if(GeometricCollisions::pointVsSphere(centerObjPosition, 
								Vector3(particles[i].x, particles[i].y, particles[i].z), particles[i].radius))
							{
							return true;
							}
						
						}
						else{ // detects collision with ships
							if(GeometricCollisions::sphereVsSphere(centerObjPosition, radiusObj, 
								Vector3(particles[i].x, particles[i].y, particles[i].z),
								particles[i].radius))
							{
							return true;
							}
						}

					}
				
				}

			}
		}
	}

	return false;
}