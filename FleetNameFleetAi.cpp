//
//  FleetNameFleetAi.cpp
//
//  All through this file, you should replace "FleetName",
//    "Fleet Name", and "FLEET_NAME" with the name of your
//     fleet.
//

#include <cassert>
#include <vector>

#include "ObjLibrary/Vector3.h"

#include "WorldInterface.h"
#include "PhysicsObjectId.h"
#include "ShipAiInterface.h"
#include "Ship.h"
#include "AiShipReference.h"
#include "UnitAiSuperclass.h"
#include "FleetNameUnitAi.h"
#include "FleetAiSuperclass.h"
#include "FleetNameFleetAi.h"

using namespace std;
using namespace Apollo_11;


namespace
{
	const double FIGHTER_DISTANCE_SCALE = 625.0;
	const Vector3 A_FIGHTER_START_POSITION[FleetAi::FIGHTER_COUNT] =
	{
		Vector3( 1.0,  0.0,  0.0),
		Vector3( 2.0,  0.0,  0.0),
		Vector3(-1.0,  0.0,  0.0),
		Vector3(-2.0,  0.0,  0.0),
		Vector3( 0.0,  1.0,  0.0),
		Vector3( 0.0,  2.0,  0.0),
		Vector3( 0.0, -1.0,  0.0),
		Vector3( 0.0, -2.0,  0.0),
		Vector3( 0.0,  0.0,  1.0),
		Vector3( 0.0,  0.0,  2.0),
		Vector3( 0.0,  0.0, -1.0),
		Vector3( 0.0,  0.0, -2.0),
	};

	const Vector3 A_FIGHTER_START_VELOCITY[FleetAi::FIGHTER_COUNT] =
	{
		Vector3( 1.0,  1.0,  0.0).getNormalized(),
		Vector3( 1.0, -1.0,  0.0).getNormalized(),
		Vector3(-1.0,  0.0,  1.0).getNormalized(),
		Vector3(-1.0,  0.0, -1.0).getNormalized(),
		Vector3( 0.0,  1.0,  1.0).getNormalized(),
		Vector3( 0.0,  1.0, -1.0).getNormalized(),
		Vector3( 1.0, -1.0,  0.0).getNormalized(),
		Vector3(-1.0, -1.0,  0.0).getNormalized(),
		Vector3( 1.0,  0.0,  1.0).getNormalized(),
		Vector3(-1.0,  0.0,  1.0).getNormalized(),
		Vector3( 0.0,  1.0, -1.0).getNormalized(),
		Vector3( 0.0, -1.0, -1.0).getNormalized(),
	};

}



FleetAi :: FleetAi ()
		: FleetAiSuperclass()
{
	//  ADD MORE INITIALIZATION IF NEEDED
}

FleetAi :: FleetAi (const FleetAi& original)
		: FleetAiSuperclass(original)
{
	assert(!original.isInitialized());

	//  ADD MORE COPYING IF NEEDED
}

FleetAi :: FleetAi (const FleetAi& original,
                    AiShipReference command_ship,
                    vector<AiShipReference>& v_fighters,
                    vector<Marker*>& vp_markers)
		: FleetAiSuperclass(original,
		                    command_ship,
		                    v_fighters,
		                    vp_markers)
{
	assert(original.isInitialized());
	assert(command_ship.isShip());
	assert(v_fighters.size() == FIGHTER_COUNT);
	for(unsigned int i = 0; i < FIGHTER_COUNT; i++)
		assert(v_fighters[i].isShip());
	assert(vp_markers.size() == MARKER_COUNT);
	for(unsigned int i = 0; i < MARKER_COUNT; i++)
		assert(vp_markers[i] != NULL);

	//  ADD MORE COPYING IF NEEDED
}

FleetAi :: ~FleetAi ()
{
	// Ships do not have to be deallocated
}



///////////////////////////////////////////////////////////////
//
//  Functions inherited from FleetAiSuperclass
//

FleetAiSuperclass* FleetAi :: getCloneUninitialized () const
{
	assert(!isInitialized());

	// This function is correct
	return new FleetAi(*this);
}

FleetAiSuperclass* FleetAi :: getCloneInitialized (AiShipReference command_ship,
                                                   vector<AiShipReference>& v_fighters,
                                                   vector<Marker*>& vp_markers) const
{
	assert(isInitialized());
	assert(command_ship.isShip());
	assert(v_fighters.size() == FIGHTER_COUNT);
	for(unsigned int i = 0; i < FIGHTER_COUNT; i++)
		assert(v_fighters[i].isShip());
	assert(vp_markers.size() == MARKER_COUNT);
	for(unsigned int i = 0; i < MARKER_COUNT; i++)
		assert(vp_markers[i] != NULL);

	// This function is correct
	return new FleetAi(*this, command_ship, v_fighters, vp_markers);
}

Vector3 FleetAi :: getFighterStartPosition (unsigned int index) const
{
	assert(index < FIGHTER_COUNT);

	//  THIS IS THE WRONG ANSWER
	//return Vector3::ZERO;  // start at the same position as the command ship
	return A_FIGHTER_START_POSITION[index] * FIGHTER_DISTANCE_SCALE;
}

Vector3 FleetAi :: getFighterStartForward (unsigned int index) const
{
	assert(index < FIGHTER_COUNT);

	//  THIS MAY BE THE WRONG ANSWER
	//return Vector3::UNIT_X_PLUS;  // same direction as command ship
	// start flying away in all directions
	return A_FIGHTER_START_VELOCITY[index];
}

/*
void FleetAi :: draw () const
{
	assert(isInitialized());

	// ADD ANY VISUAL DEBUGGING HERE
}
*/


void FleetAi :: init (const WorldInterface& world,
                      unsigned int fleet,
                      AiShipReference command_ship,
                      vector<AiShipReference>& v_fighters,
                      vector<Marker*>& vp_markers)
{
	assert(!isInitialized());
	assert(fleet <= PhysicsObjectId::FLEET_MAX);
	assert(command_ship.isShip());
	assert(v_fighters.size() == FIGHTER_COUNT);
	for(unsigned int i = 0; i < FIGHTER_COUNT; i++)
		assert(v_fighters[i].isShip());
	assert(vp_markers.size() == MARKER_COUNT);
	for(unsigned int i = 0; i < MARKER_COUNT; i++)
		assert(vp_markers[i] != NULL);

	setShipsAndMarkers(command_ship, v_fighters, vp_markers);

	//  ASSIGN UNIT AIS HERE

	PhysicsObjectId moon_id = world.getNearestPlanetoidId(command_ship.getShip().getPosition());
	getCommandShipAi().setUnitAi(new Apollo_11::UnitAiMoonGuard(getCommandShipAiShipReference(), world, moon_id));


	for(unsigned int i = 0; i < FIGHTER_COUNT; i++){
		getFighterAi(i).setUnitAi(new Apollo_11::UnitAiMoonGuard(getFighterAiShipReference(i), world, moon_id));
		//getCommandShipAi().setUnitAi(new Apollo_11::UnitAiMoonGuard(getCommandShipAiShipReference(), world));
	}

	assert(isInitialized());
	//assert(invariant());
}

void FleetAi :: run (const WorldInterface& world)
{
	assert(isInitialized());

	//
	//  THIS FUNCTION MAY NEED MORE UPDATES
	//    -> assigning new unit AIs to Ships
	//    -> modifying Markers
	//

	//  This just runs all the unit AIs every frame.  Change it
	//    if you need something different.
	getCommandShipAi().runAi(world);
	for(unsigned int i = 0; i < FIGHTER_COUNT; i++)
		getFighterAi(i).runAi(world);

	//assert(invariant());
}



///////////////////////////////////////////////////////////////
//
//  Helper function not inherited from anywhere
//

//
//  Assignment Operator
//
//  This function has intentionally not been implemented.
//
//  FleetAi& FleetAi :: operator= (const FleetAi& original)
//
