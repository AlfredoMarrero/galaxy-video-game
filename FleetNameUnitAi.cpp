//
//  FleetNameUnitAi.h
//
//  All through this file, you should replace "FleetName",
//    "Fleet Name", and "FLEET_NAME" with the name of your
//     fleet.
//


#include <cassert>
#include "UnitAiSuperclass.h"
#include "FleetNameUnitAi.h"
#include "Bullet.h"
#include "WorldInterface.h"
#include "PhysicsObjectId.h"
#include "PhysicsObject.h"
#include "ShipAiInterface.h"
#include "Ship.h"

#include <vector>


//using namespace FleetName;
using namespace Apollo_11;
using namespace std;


Vector3 fighterPosition [12] = {
	Vector3(0, 0 , 500),
	Vector3(0, 0 , 1000),
	Vector3(0, 0 , 1500),

	Vector3(0, 500, 0),
	Vector3(0, 1000, 0),
	Vector3(0, 1500, 0),

	Vector3(500, 0, 0),
	Vector3(1000, 0, 0),
	Vector3(1500, 0, 0),

	Vector3(1000, 300, 0),
	Vector3(0, 300, 800),
	Vector3(1000, 0, 1000)
};








UnitAiMoonGuard::UnitAiMoonGuard(const AiShipReference& ship,
	const WorldInterface& world,
	const PhysicsObjectId& id_moon
	)
	: UnitAiSuperclass(ship),  // invoke constructor from base class
	m_steering_behaviour(ship.getId()),  // no default constructor available
	m_scan_counter(rand() % SCAN_COUNT_MAX), // ships shouldn't all scan on the same frame
	m_id_target(id_moon)  // no target yet
{
		assert(ship.isShip());
}


UnitAiMoonGuard::UnitAiMoonGuard(const UnitAiMoonGuard& original,
	const AiShipReference& ship)
	: UnitAiSuperclass(ship),
	m_steering_behaviour(original.m_steering_behaviour), // no default constructor available
	m_scan_counter(original.m_scan_counter), // ships shouldn't all scan on the same frame
	m_id_target(original.m_id_target) // ID Moon

									  //m_steering_behaviour(ship.getId())
{
	assert(ship.isShip());

	// MORE INITIALIZATION NEEDED
}




UnitAiMoonGuard :: ~UnitAiMoonGuard()
{


}


//
//  run
//
//  Purpose: To run this UnitAiSuperclass once.  This function
//           will set the desired velocity for the Ship and mark
//           the weapons to fire if that desired.  This function
//           must work correctly even if it is not called every
//           frame.
//  Parameter(s):
//    <1> world: The World that the Ship is in
//  Precondition(s):
//    <1> world.isAlive(getShipId())
//  Returns: N/A
//  Side Effect: The desired velocity for the controlled Ship is
//               updated.  Any weapons that should be fired are
//               marked acccordingly.
//

//void UnitAiMoonGuard::run(const WorldInterface& world)
//{
//
//	
//	//cout<<"world.isAlive(getShipId())  "<<world.isAlive(getShipId())<< endl;
//
//	if(!world.isAlive(getShipId()))
//		m_scan_counter++;  // pointless opeRation for breakpoint
//
//    assert(world.isAlive(getShipId()));
//
//	//
//	// Scan for enemies evey few frames. We use a counter to
//	// know which frames to scan on.
//	//
//
//	m_scan_counter++;
//	if (m_scan_counter >= SCAN_COUNT_MAX)
//	{
//		chooseTarget(world);
//		m_scan_counter = 0;
//	}
//
//	//
//	// Check to make sure the target is still alive. If we
//	// didn't scan this frame, it might have died since we
//	// last checked.
//	//
//
//	if (m_id_target != PhysicsObjectId::ID_NOTHING &&
//		!world.isAlive(m_id_target))
//	{
//		// give up on dead target
//		m_id_target = PhysicsObjectId::ID_NOTHING;
//	}
//
//	//
//	// If we have a target, shoot at it! Otherwise, just stop and
//	// wait. If we don't move while there is no target, we
//	// can't hit anything, so we won't die.
//	//
//
//	if (m_id_target != PhysicsObjectId::ID_NOTHING)
//	{
//		//cout<<"llego a aqui sin id en FleetName"<< endl;
//		// if we have a living target
//		assert(world.isAlive(m_id_target));
//
//		steerToRamTarget(world);
//		shootAtTarget(world);
//	}
//	else
//	{
//	 
//
//		PhysicsObjectId id = world.getNearestPlanetoidId(getShip().getPosition());
//		double planetRadius = world.getRadius(id);
//		Vector3 planetCenter = world.getPosition(id);
//
//		Vector3 desiredVelocity;
//		if(world.isShipCommandShip(getShipId()) && world.isAlive(getShipId()))
//		{
//			//getShipAi().setDesiredVelocity(Vector3::ZERO);
//
//			desiredVelocity = m_steering_behaviour.patrolSphere(world, planetCenter, planetRadius, PLANETOID_AVOID_DISTANCE);
//		}
//		else if(world.isAlive(getShipId()))
//		{
//
//
//			//if(getShipId().isAlive())
//		 // cout<< "Llego aqui  1"<< world.isAlive(getShipId())<<endl;
//
//			//
//			PhysicsObjectId commandShipID = world.getFleetCommandShipId(getShipId().m_fleet);
//			//cout<< "Llego aqui  2"<< world.isAlive(getShipId())<<endl;
//			Vector3 commandShipPosition = world.getPosition(commandShipID);
//			//cout<< "Llego aqui  3"<< world.isAlive(getShipId())<<endl;
//			//cout<<"Command ship position: " <<  world.getPosition(commandShipID)<< endl;
//			//cout<<"Command ship position: " <<  world.getPosition(commandShipID)<< endl;
//			unsigned int fighterIndex= getShipId().m_index;
//			Vector3 arrivePosition = commandShipPosition +  fighterPosition [fighterIndex];
//			
//			//Vector3 arrive (const WorldInterface& world, const Vector3& target_position);
//			desiredVelocity = m_steering_behaviour.arrive(world, arrivePosition);
//
//		}
//
//		Vector3 avoidVeloc = m_steering_behaviour.avoid(world, desiredVelocity, planetCenter, planetRadius, PLANETOID_CLEARANCE, PLANETOID_AVOID_DISTANCE);
//
//		vector<WorldInterface::RingParticleData> ringParticleData (world.getRingParticles (getShip().getPosition(), 350));
//
//		for(unsigned int i = 0 ; i < ringParticleData.size(); i++){
//		
//			avoidVeloc = m_steering_behaviour.avoid ( world,
//						avoidVeloc,
//						ringParticleData[i].m_position,
//						ringParticleData[i].m_radius,
//						PLANETOID_CLEARANCE,
//						350.0);
//		}
//
//
//  //cout<<"Desired Velocity  from FleetNameUnitAi "<<avoidVeloc<< endl;
//		getShipAi().setDesiredVelocity(avoidVeloc);
//	}
//}





void UnitAiMoonGuard::run(const WorldInterface& world)
{

	
	//cout<<"world.isAlive(getShipId())  "<<world.isAlive(getShipId())<< endl;

	if(!world.isAlive(getShipId()))
		m_scan_counter++;  // pointless opeRation for breakpoint

    assert(world.isAlive(getShipId()));

	//
	// Scan for enemies evey few frames. We use a counter to
	// know which frames to scan on.
	//

	m_scan_counter++;
	if (m_scan_counter >= SCAN_COUNT_MAX)
	{
		chooseTarget(world);
		m_scan_counter = 0;
	}

	//
	// Check to make sure the target is still alive. If we
	// didn't scan this frame, it might have died since we
	// last checked.
	//

	if (m_id_target != PhysicsObjectId::ID_NOTHING &&
		!world.isAlive(m_id_target))
	{
		// give up on dead target
		m_id_target = PhysicsObjectId::ID_NOTHING;
	}

	//
	// If we have a target, shoot at it! Otherwise, just stop and
	// wait. If we don't move while there is no target, we
	// can't hit anything, so we won't die.
	//

	if (m_id_target != PhysicsObjectId::ID_NOTHING)
	{
		//cout<<"llego a aqui sin id en FleetName"<< endl;
		// if we have a living target
		assert(world.isAlive(m_id_target));

		steerToRamTarget(world);
		shootAtTarget(world);
	}
	else
	{
	 

		PhysicsObjectId id = world.getNearestPlanetoidId(getShip().getPosition());
		double planetRadius = world.getRadius(id);
		Vector3 planetCenter = world.getPosition(id);
			Vector3 desiredVelocity;
	
		//if(world.isShipCommandShip(getShipId()) && world.isAlive(getShipId()))
		if(world.isShipCommandShip(getShipId()))
		{
			//getShipAi().setDesiredVelocity(Vector3::ZERO);
			//cout<<"Llego comand ship"<< getShipId().m_fleet<<"   "<< getShipId().m_index <<endl;

			desiredVelocity = m_steering_behaviour.patrolSphere(world, planetCenter, planetRadius, PLANETOID_AVOID_DISTANCE);

		//	cout<<"Command ship desired velocity: "<<desiredVelocity<< endl;
			//getShipAi().setDesiredVelocity(desiredVelocity);
			//return;
		}
		//else if(world.isAlive(getShipId()))
		else
		{


			//if(getShipId().isAlive())
		 // cout<< "Llego aqui  1"<< world.isAlive(getShipId())<<endl;

			//
			PhysicsObjectId commandShipID = world.getFleetCommandShipId(getShipId().m_fleet);
			//cout<< "Llego aqui  2"<< world.isAlive(getShipId())<<endl;
			Vector3 commandShipPosition = world.getPosition(commandShipID);
			//cout<< "Llego aqui  3"<< world.isAlive(getShipId())<<endl;
			//cout<<"Command ship position: " <<  world.getPosition(commandShipID)<< endl;
			//cout<<"Command ship position: " <<  world.getPosition(commandShipID)<< endl;
			unsigned int fighterIndex= getShipId().m_index;
			Vector3 arrivePosition = commandShipPosition +  fighterPosition [fighterIndex];
			
			//Vector3 arrive (const WorldInterface& world, const Vector3& target_position);
			desiredVelocity = m_steering_behaviour.arrive(world, arrivePosition);


			//cout<<"Fighter ship desired velocity: "<<desiredVelocity<< endl;

			//getShipAi().setDesiredVelocity(desiredVelocity);
			//return;
			
		}

		//cout<<"before: "<<desiredVelocity<<endl;
		avoidShips(world, desiredVelocity);
		//cout<<"after: "<<desiredVelocity<<endl;

		//Vector3 avoidVeloc = m_steering_behaviour.avoid(world, desiredVelocity, planetCenter, planetRadius, PLANETOID_CLEARANCE, PLANETOID_AVOID_DISTANCE);

		//vector<WorldInterface::RingParticleData> ringParticleData (world.getRingParticles (getShip().getPosition(), 350));

		//for(unsigned int i = 0 ; i < ringParticleData.size(); i++){
		//
		//	avoidVeloc = m_steering_behaviour.avoid ( world,
		//				avoidVeloc,
		//				ringParticleData[i].m_position,
		//				ringParticleData[i].m_radius,
		//				PLANETOID_CLEARANCE,
		//				350.0);
		//}

		//avoidShips(world, desiredVelocity);
		getShipAi().setDesiredVelocity(desiredVelocity);
		//getShipAi().setDesiredVelocity(desiredVelocity);
	}
	


}




void UnitAiMoonGuard::avoidShips(const WorldInterface& world, Vector3& avoidVeloc)
{
	//avoid each ship
	PhysicsObjectId thisShipId = getShipId();
	vector<PhysicsObjectId> ship_id(world.getShipIds(getShip().getPosition(), getShip().getRadius()));
	if (world.isAlive(thisShipId ))
	{

		for (unsigned int i = 0; i < ship_id.size(); i++)
		{
			if (ship_id[i] != thisShipId )
			{
				avoidVeloc = m_steering_behaviour.avoid(world,
					avoidVeloc,
					world.getPosition(ship_id[i]),
					world.getRadius(ship_id[i]),
					world.getRadius(ship_id[i]) + 10,
					350);
			}
			

		}
	}
}



void UnitAiMoonGuard::chooseTarget(const WorldInterface& world)
{
	const Vector3& ship_position = getShip().getPosition();
	const Vector3& ship_forward = getShip().getForward();
	unsigned int   ship_fleet = getShipId().m_fleet;

	vector<PhysicsObjectId> v_ids = world.getShipIds(ship_position, SCAN_RADIUS);



	m_id_target = PhysicsObjectId::ID_NOTHING;
	double best_angle = VERY_LARGE_ANGLE;

	for (unsigned int i = 0; i < v_ids.size(); i++)
	{
		PhysicsObjectId other_id = v_ids[i];

		if (other_id.m_fleet != ship_fleet)
		{
			// we found an enemy ship

			Vector3 other_position = world.getPosition(other_id);
			Vector3 direction_to_other = other_position - ship_position;
			double angle_to_other = direction_to_other.getAngleSafe(ship_forward);

			if (angle_to_other < best_angle)
			{
				m_id_target = other_id;
				best_angle = angle_to_other;
			}
		}
		// else ship is part of our fleet, so ignore it
	}
}


void UnitAiMoonGuard::steerToRamTarget(const WorldInterface& world)
{
	assert(m_id_target != PhysicsObjectId::ID_NOTHING);
	assert(world.isAlive(m_id_target));
	//vector<WorldInterface::RingParticleData> t = world.getRingParticles(getShip().getPosition(), getShip().getRadius());
	PhysicsObjectId planetoid_id = world.getNearestPlanetoidId(getShip().getPosition());
	Vector3         planetoid_center = world.getPosition(planetoid_id);
	double          planetoid_radius = world.getRadius(planetoid_id);

	Vector3 desired_velocity = m_steering_behaviour.aim(world, m_id_target,Bullet::SPEED);
	//Vector3 avoid_velocity;
	Vector3 avoid_velocity = m_steering_behaviour.avoid(world,
		desired_velocity,
		planetoid_center,
		planetoid_radius,
		PLANETOID_CLEARANCE,
		PLANETOID_AVOID_DISTANCE);


	getShipAi().setDesiredVelocity(avoid_velocity);
}

void UnitAiMoonGuard::shootAtTarget(const WorldInterface& world)
{
	assert(m_id_target != PhysicsObjectId::ID_NOTHING);
	assert(world.isAlive(m_id_target));

	Vector3 target_position = world.getPosition(m_id_target);
	Vector3 target_velocity = world.getVelocity(m_id_target);
	Vector3 aim_direction = m_steering_behaviour.getAimDirection(getShip().getPosition(),
		Bullet::SPEED,
		target_position,
		target_velocity);
	double angle = aim_direction.getAngleSafe(getShip().getVelocity());

	if (angle <= SHOOT_ANGLE_RADIANS_MAX)
	{
		getShipAi().markFireBulletDesired();
	}
}




///////////////////////////////////////////////////////////////
//
//  Virtual functions from UnitAiSuperclass
//

UnitAiSuperclass* UnitAiMoonGuard::getCloneForShip(const AiShipReference& ship) const
{
	assert(ship.isShip());

	return new UnitAiMoonGuard(*this, ship);
}

void UnitAiMoonGuard::draw() const
{
	; // do nothing
}

void UnitAiMoonGuard::updateForPause()
{
	; // do nothing
}




//
//
//void UnitAiMoonGuard::chooseTarget(const WorldInterface& world)
//{
//	const Vector3& ship_position = getShip().getPosition();
//	const Vector3& ship_forward = getShip().getForward();
//	unsigned int   ship_fleet = getShipId().m_fleet;
//
//	vector<PhysicsObjectId> v_ids = world.getShipIds(ship_position, SCAN_RADIUS);
//
//	m_id_target = PhysicsObjectId::ID_NOTHING;
//	double best_angle = VERY_LARGE_ANGLE;
//
//	for (unsigned int i = 0; i < v_ids.size(); i++)
//	{
//		PhysicsObjectId other_id = v_ids[i];
//
//		if (other_id.m_fleet != ship_fleet)
//		{
//			// we found an enemy ship
//
//			Vector3 other_position = world.getPosition(other_id);
//			Vector3 direction_to_other = other_position - ship_position;
//			double angle_to_other = direction_to_other.getAngleSafe(ship_forward);
//
//			if (angle_to_other < best_angle)
//			{
//				m_id_target = other_id;
//				best_angle = angle_to_other;
//			}
//		}
//		// else ship is part of our fleet, so ignore it
//	}
//
//
//
//
//}





///////////////////////////////////////////////////////////////
//
//  Helper functions not inherited from anywhere
//

//
//  Copy Constructor
//  Assignment Operator
//
//  These functions have intentionally not been implemented.
//    Use the modified copy constructor instead.
//
//  UnitAiMoonGuard :: UnitAiMoonGuard (const UnitAiMoonGuard& original);
//  UnitAiMoonGuard& UnitAiMoonGuard :: operator= (const UnitAiMoonGuard& original);
//






