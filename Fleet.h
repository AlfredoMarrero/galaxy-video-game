



#ifndef FLEET_H
#define FLEET_H

#include "PhysicsObjectId.h"
#include "WorldInterface.h"
#include "ShipAiInterface.h"
#include "Ship.h"
#include "AiShipReference.h"
#include "UnitAiSuperclass.h"
#include "FleetNameSteeringBehaviours.h"
#include "Bullet.h"
//files added A6
#include "FleetAiSuperClass.h"
#include "Marker.h"
#include "TimeSystem.h"
#include "FleetNameFleetAi.h"



// 1 MISSING THE FLET AI
//Each fleet should have a fleet AI, 13 ships, and an array of bullets.  
//The fleet AI is explained in detailed below. In the fleet class, 
//it should be represented as a pointer of type FleetAiSuperclass *.
//It can be set to NULL for now. If your program has missiles, each 
//fleet should also have an array of those. 

//2 Done
//You must also download the Marker class from the course website 
//and add an array of Markers to each fleet as well.  A Marker is a 
//PhysicsObject that is not displayed; it is always alive and moves 
//according to its velocity.  The FleetAiSuperclass needs the markers 
//to compile, but you do not have to do anything with them if you do
//not need them.

//3 Done
//As mentioned, a fleet has 13 ships. Twelve of these ships are fighters,
//and have exactly the same characteristics as the enemy ships in earlier
//assignments.  The 13th ship is the command ship.  It has a radius of 25 m,
//10 health, a maximum speed of 200 m/s, an acceleration of 150 m/s2, and a
//maximum rotation rate of 1/4 pi radians per second (45 degrees / second).
//It should also have 8 ammunition, which will be used for missiles if your
//program has them.

//4 Done
//A fleet should be created either above or below the rings (50% chance of each). 
//Specifically, each command ship should be spawned with a random distance of
//between 90,000 m and 140,000 m from the origin on the XZ plane 
//(hint: getRandomUnitVectorXZ) and a Y coordinate of positive or negative 15,000 m. 

//5 
//The newly-spawned command ship should have a random orientation

//6
//To obtain a starting positions and forward vector for a fighter, call the getFighterStartPosition
//and getFighterStartForward functions in the fleet AI.  These functions return results that are 
//expressed in the local coordinate system for the command ship.  To convert to global coordinates,
//use the following formulas:
//
//Vector3 position = command_ship_center +
//                   offset_local.x * command_ship_forward +
//                   offset_local.y * command_ship_up +
//                   offset_local.z * command_ship_right;
//
//Vector3 forward = forward_local.x * command_ship_forward +
//                  forward_local.y * command_ship_up +
//                  forward_local.z * command_ship_right;


using namespace Apollo_11;
const int NUMBER_OF_SHIPS = 13;
const int NUMBER_OF_BULLETS = 100;
const int NUMBER_OF_MARKERS = 10;

class Fleet
{
public:
	Fleet();
	~Fleet();

	//void initShips();
	void initShips(int fleet_index, WorldInterface& world);
	void draw() const;
	PhysicsObjectId getCommandShipId() const;
	Ship& getShip(int index);
	const Ship& getShip(int index) const;
	void update(WorldInterface& world);

private:

	Ship ship[NUMBER_OF_SHIPS];
	Bullet bullet[NUMBER_OF_BULLETS];
	FleetAiSuperclass *p_fas;
	Marker marker[NUMBER_OF_MARKERS];
	DisplayList shipDL;
	
};



#endif