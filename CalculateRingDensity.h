//
// ALfredo Marrero
// 200 330 199
// interface for class CoordinateSystem
// CalculateRingDensity.h

#include "ObjLibrary/Vector3.h"
#include "ObjLibrary/ObjModel.h"
#include "GetGlut.h"
#include "ObjLibrary/DisplayList.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include <iostream>

using namespace std;

// preventing multiple declarations
#ifndef CALCULATE_RING_DENSITY_H
#define CALCULATE_RING_DENSITY_H

struct position{

	int x, y, z;
};


struct MoonsAndPlanet{
	double radius;
	double x;
	double z;
};

const int NumberOfMoonsAndP = 11;

// class for the camera
class CalculateRingDensity{

public:
	// construtor
	CalculateRingDensity();
	// functions
	double rawDensityForP(Vector3 p);
	double rawDensityWithRespectToHole(Vector3 p1);
	int curvedDensityFromMinRawDesity(Vector3 p1);

private:
	//member variables
	int ringY_halfLenght;
	int ringXZdistance;
	int planetRadius;
	MoonsAndPlanet moons[NumberOfMoonsAndP];
};

#endif