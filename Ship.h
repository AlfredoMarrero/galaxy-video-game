

#include "ObjLibrary/Vector3.h"
#include "GetGlut.h"
#include "PhysicsObject.h"
#include "ObjLibrary/Vector3.h"
#include "CoordinateSystem.h"
#include "PhysicsObjectId.h"
#include "ShipManeuverInterface.h"
#include "ShipAiInterface.h"
#include "WorldInterface.h"
#include "UnitAiSuperclass.h"


//const float lowHealth = 0.5;
//#include "TimeSytem.h" 
using namespace std;

//const double MAX_SPEED = 2500;

// preventing multiple declarations
#ifndef SHIP_H
#define SHIP_H


class Ship : public PhysicsObject, public ShipAiInterface, public ShipManeuverInterface
{

public:
	//
	//  RADUIS
	//
	//  The default radius of the sphere used to represent a Bullet
	//    for collision checking.
	//

	static const double RADIUS;

	//
	//  LIFESPAN
	//
	//  The time between when a Bullet is created and when it
	//    disappears.
	//

	static const double LIFESPAN;

//
//  HEALTH_DEAD_AT
//
//  A constant indicating the health value at or below which a
//    Ship is considered to be dead.  This value is slightly
//    more than 0.0, and is used to avoid the effects of
//    floating-point rounding errors.
//

	static const float HEALTH_DEAD_AT;
	//
	//  SPEED
	//
	//  The speed a Bullet flies at.
	//

	static const double SPEED;


	Ship();
	Ship(const Ship& original);

	Ship(const PhysicsObjectId& id, const Vector3& position, const Vector3& forward,
		const DisplayList& display_list, double display_scale, float health,
		int ammo, double SpeedMax,
		double Acceleration,
		double RotationRate);


	Ship& operator= (const Ship& original);

	~Ship();





///////////////////////////////////////////////////////////////
//
//  Virtual functions inherited from PhysicsObject
//

//
//  getClone
//
//  Purpose: To create a dynamically allocated copy of this
//           PhysicsObject.
//  Parameter(s): N/A
//  Precondition(s): N/A
//  Returns: A deep copy of this PhysicsObject.  This copy
//           should be created using the copy constructor for
//           the derived class.
//  Side Effect: N/A
//
		
	virtual PhysicsObject* getClone () const;

//
//  isAlive
//
//  Purpose: To determine if this PhysicsObject is alive.
//  Parameter(s): N/A
//  Precondition(s): N/A
//  Returns: Whether this PhysicsObject is alive.  If this
//           PhysicsObject is of a type that cannot become dead,
//           true is returned.
//  Side Effect: N/A
//

	virtual bool isAlive () const;

//
//  isDying
//
//  Purpose: To determine if this PhysicsObject is currently
//           dying.  This function can be used to prevent a
//           PhysicsObject being counted twice.  For example, a
//           bullet should not hit the same target twice.
//  Parameter(s): N/A
//  Precondition(s):
//    <1> isAlive()
//  Returns: Whether this PhysicsObject is going to be marked as
//           dead the next time that update is called.  If this
//           PhysicsObject is of a type that cannot become dead,
//           false is returned.
//  Side Effect: N/A
//

	virtual bool isDying () const;

//
//  markDead
//
//  Purpose: To mark this PhysicsObject as dead.  Its creation
//           time is set to somewhere in the distant past. 
//  Parameter(s):
//    <1> instant: Whether the PhysicsObject should be marked as
//                 dead immediately
//  Precondition(s): N/A
//  Returns: N/A
//  Side Effect: If this PhysicsObject is of a type that cannot
//               become dead, there is no effect. Otherwise, if
//               instant == true, this PhysicisObject is
//               marked as dead and no death actions - such as
//               generating a death explosion - are performed.
//               If this PhysicsObject can become dead and
//               instant == false, this PhysicsObject is marked
//               as dying.  The next time that update is called,
//               any death actions - such as generating a death
//               explosion - will be performed and this
//               PhysicsObject will be marked as dead.
//

	virtual void markDead (bool instant);

//
//  update
//
//  Purpose: To update this PhysicsObject for one frame.  This
//           function does not perform collision checking or
//           handling.
//  Parameter(s):
//    <1> r_world: An interface to the world this PhysicsObject
//                 is in
//  Precondition(s): N/A
//  Returns: N/A
//  Side Effect: This PhysicsObject is updated for one frame.
//               Any queries about or changes to the world it is
//               in are resolved through r_world.
//

	virtual void update (WorldInterface& r_world);




//
//  invariant
//
//  Purpose: To determine if the class invariant is true.
//  Parameter(s): N/A
//  Precondition(s): N/A
//  Returns: Whether the class invariant is true.
//  Side Effect: N/A
//

	bool invariant () const;





		// virtual functions from ShipAManeuverInterface();
		virtual double getAcceleration() const;
		virtual double getRotationRate() const;
		virtual void setManeuverability(double speed_max, double acceleration, double rotation_rate);
		virtual double getSpeedMax () const;
		

		//virtual functions from ShipAiInterface
		virtual bool isUnitAiSet() const;
		virtual UnitAiSuperclass& getUnitAi() const;
		virtual void drawUnitAi() const;
		virtual void setDesiredVelocity(const Vector3& desired_velocity);
		virtual void markFireBulletDesired();
		virtual void markFireMissileDesired(const PhysicsObjectId& id_target);
		virtual void runAi(const WorldInterface& world);
		virtual void setUnitAi(UnitAiSuperclass* p_unit_ai);
		virtual void setUnitAiNone();

		//HELPER FUNCTIONS
		void reduceHealth(int amount);
		float getHealth() const;
		bool isReloaded() const;
		int getAmmo() const;
		void setupCamera() const;
		CoordinateSystem getCameraCoordinateSystem() const; void markReloading();
		void markNotReloading();
		void setHealth(float health);
		void setAmmo(int ammo);
		void addAmmo(int increase);

private:
	bool fire;
	UnitAiSuperclass* unitAI;
	Vector3 DesiredVelocity;
	float s_health;
	CoordinateSystem CameraCoordinateSystem;
	PhysicsObjectId shipID;
	int ammoCount;
	bool isdead;
	double creationtime;
	double reloadTimer;


	//member variables for ShipManeuverInterface 
	double maximum_speed;
	double acceleration;
	double rotation_rate;



};



#endif











