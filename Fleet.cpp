#include "Fleet.h"
#include <cassert>
#include <iostream>
#include "ObjLibrary/Vector3.h"
#include "PhysicsObjectId.h"
#include "PhysicsObject.h"
#include "ExplosionManagerInterface.h"
#include "ExplosionManager.h"
#include "WorldInterface.h"
#include "World.h"

#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include "TimeSystem.h"


Fleet::Fleet(){

	p_fas = NULL;
}

Fleet::~Fleet(){}


void Fleet::initShips(int fleet_index, WorldInterface& world){


	p_fas = new Apollo_11::FleetAi();

	ObjModel shipObjModel;
	shipObjModel.load("Models/Grapple.obj");
	shipDL = shipObjModel.getDisplayList();


	//COMMAND  SHIP
	Vector3 commandShipPosition = Vector3::getRandomUnitVectorXZ();

     /* initialize random seed: */
      srand (time(NULL) * fleet_index);
	double randomDistance = rand() % (140000 - 90000) + 90000;
	   /* initialize random seed: */
      srand (time(NULL)/fleet_index);
	double xRandom = rand() % 140000;
	double zRandom = sqrt(randomDistance*randomDistance - xRandom* xRandom);
	Vector3 UnitVecRand =  Vector3::getRandomUnitVectorXZ();

	
	commandShipPosition.x =UnitVecRand.x *  xRandom;
	(int(xRandom) % 2) == 0? commandShipPosition.y = 15000: commandShipPosition.y = -15000;
	//playerPosition.y = moons[8].radius + 500;
	commandShipPosition.z = UnitVecRand.z * zRandom;

	//cout<<"Command ship:  "<<fleet_index  <<" position: "<<commandShipPosition<< endl;

	//PhysicsObjectId playerID = PhysicsObjectId (PhysicsObjectId::FLEET_PLAYER,fleet_index, NUMBER_OF_SHIPS - 1);
	PhysicsObjectId playerID = PhysicsObjectId (PhysicsObjectId::TYPE_SHIP,fleet_index, NUMBER_OF_SHIPS - 1);

	//// for debuggin purpose
	////if(fleet_index == 3){
	//commandShipPosition.x =  -90000.0;
	//commandShipPosition.y =19500.0;
	//commandShipPosition.z = -40000.0;
	////}

	ship[NUMBER_OF_SHIPS - 1].setManeuverability(200.0, 150.0, (1.0 / 4.0) * 3.14);
	//Vector3 playerShipVelocity = SPEED * ship[NUMBER_OF_SHIPS].getForward();
	Vector3 velocity(0,0,0);

	Vector3 velocityCommandShip = SPEED * ship[NUMBER_OF_SHIPS - 1].getForward();
	ship[NUMBER_OF_SHIPS - 1].initPhysics(playerID, commandShipPosition, 25.0, velocityCommandShip, shipDL, 10.0);
	ship[NUMBER_OF_SHIPS - 1].setHealth(10.0);
	ship[NUMBER_OF_SHIPS - 1].setAmmo(8.0);



	//SHIPS
	for(int i = 0; i < NUMBER_OF_SHIPS - 1; i++){
		PhysicsObjectId id = PhysicsObjectId (PhysicsObjectId::TYPE_SHIP, fleet_index, i);

		Vector3 startPosition = p_fas ->getFighterStartPosition(i);
		Vector3 forwardPosition = p_fas -> getFighterStartForward(i);


		Vector3 position = ship[NUMBER_OF_SHIPS - 1].getPosition()+ 
			startPosition.x * ship[NUMBER_OF_SHIPS - 1].getForward()+ 
			startPosition.y * ship[NUMBER_OF_SHIPS - 1].getUp() + 
			startPosition.z * ship[NUMBER_OF_SHIPS - 1].getRight();


		Vector3 forward = forwardPosition.x * ship[NUMBER_OF_SHIPS - 1].getForward() +
			              forwardPosition.y * ship[NUMBER_OF_SHIPS - 1].getUp() +
						  forwardPosition.z * ship[NUMBER_OF_SHIPS - 1].getRight();

		
		ship[i].initPhysics(id,position, 20.0, velocity, shipDL, 10.0);
		ship[i].setHealth(1);

		ship[i].setManeuverability(250.0, 25.0, (1.0 / 3.0) * 3.14);


		//ship[i].setUnitAi(new Apollo_11::UnitAiMoonGuard(ship[i], *this, planetoidMoon[i%10].getId()));
	}

	// init fleet AI
	vector<AiShipReference> fighters;
	vector<Marker*> markers;
	for(int i = 0; i < NUMBER_OF_SHIPS - 1; i++){
	
		fighters.push_back(ship[i]);	
	}

	for(int i = 0; i < NUMBER_OF_MARKERS; i++){
	
		markers.push_back(&(marker[i]));	
	}

	

	p_fas->init(world, fleet_index, ship[NUMBER_OF_SHIPS - 1], fighters, markers);
}



//void Fleet:: draw (const Vector3& camera_forward, const Vector3& camera_up) const


void Fleet::draw() const {


    // draw player ship
	for(int i = 0; i < NUMBER_OF_SHIPS; i++){
		//cout<<"ship number :"<< i <<"  is "<<ship[i].isAlive() <<endl;
		if(ship[i].isAlive()){
			//cout<<"Ship number: "<<i<<" draw at psotion: " <<ship[i].getPosition()<< endl;
			ship[i].draw();
		}
	}
	


	if(ship[NUMBER_OF_SHIPS - 1].isAlive()){
			glPushMatrix();
		Vector3 center = ship[NUMBER_OF_SHIPS - 1].getPosition();
		//cout << "----------------------------------------------   --  - Fleet at " << center << endl;
		glTranslated(center.x, center.y, center.z);
		glColor3d(0.0, 0.0, 1.0);
		glutWireSphere(1000, 16, 12);
			glPopMatrix();

	for (int i = 0; i < NUMBER_OF_SHIPS - 1; i ++){
			glPushMatrix();
		Vector3 center = ship[i].getPosition();
		//cout << "----------------------------------------------   --  - Fleet at " << center << endl;
		glTranslated(center.x, center.y, center.z);
		glColor3d(0.0, 0.0, 1.0);
		glutWireSphere(100, 16, 12);
	glPopMatrix();
	}
	}

		
	// draw ships 
}



PhysicsObjectId Fleet::getCommandShipId() const{


	return ship[NUMBER_OF_SHIPS - 1].getId();

}


const Ship& Fleet::getShip(int index) const{

	return ship[index];

}

Ship& Fleet::getShip(int index){

	return ship[index];

}

void Fleet:: update(WorldInterface& world)
{
	
	//mp_explosion_manager->update();

	//if(ship[NUMBER_OF_SHIPS-1].isAlive()){
	//	ship[NUMBER_OF_SHIPS].update(world);
	//}


	//if(ship[NUMBER_OF_SHIPS - 1].isAlive()){
	for(unsigned int i = 0; i < NUMBER_OF_SHIPS; i++){
		

		if(!ship[NUMBER_OF_SHIPS - 1].isAlive()){
		
		    ship[i].markDead (false);
		
		}
		
		else if(ship[i].isAlive()){
			ship[i].update(world);
			if(ship[i].isAlive())
			{
				ship[i].runAi(world);
			}
		}
	}
	//}
	for(int i = 0; i < NUMBER_OF_BULLETS; i++){
		if(bullet[i].isAlive())
			bullet[i].update(world);
	}

	//collisionHandling();
	//collisionHandling();
}



