//
//  Alfredo Marrero
//  main.cpp



#include <cassert>
#include <cmath>
#include <string>
#include <iostream>
#include "GetGlut.h"
#include "Sleep.h"
#include "ObjLibrary/Vector3.h"
#include "ObjLibrary/ObjModel.h"
#include "CoordinateSystem.h"
#include "DrawRingParticles.h"
#include "ObjLibrary/DisplayList.h"
#include "TimeSystem.h"
#include "World.h"
#include "WorldInterface.h"
#include "Ship.h"
#include "Bullet.h"
#include "Planetoid.h"
#include "GeometricCollisions.h"
# pragma comment (lib, "SteeringBehaviours.lib")

World world;

// function prototype
void initDisplay();
void keyboard(unsigned char key, int x, int y);
void special(int special_key, int x, int y);
void update();
void reshape(int w, int h);
void display();
void init();
void rotateCameraToVector(const Vector3& target_facing,  double max_radians);
void specialUp(int special_key, int x, int y);
void keyboardUp(unsigned char key, int x, int y);
void collisionHandling();
void collisionHandlingForShip(Ship& ship, int i);
void collisionHandlingForBullet(Vector3 bulletPosition, int i);

DrawRingParticles collisionCheck;//d

//creating an instance of class CoordinateSystem
CoordinateSystem coordinateSystem;//d

const unsigned int KEY_UP_ARROW = 256;
const unsigned int KEY_DOWN_ARROW = 257;
const unsigned int KEY_LEFT_ARROW= 253;
const unsigned int KEY_RIGHT_ARROW = 254;
const unsigned int KEY_COUNT = 258;
const unsigned int KEY_END = 251;
const unsigned int KEY_A = 252;
const unsigned int KEY_S = 249;
const unsigned int KEY_W = 250;
const unsigned int KEY_D = 241;
const unsigned int KEY_F = 242;
const unsigned int KEY_SPACE = 243;
const unsigned int KEY_BACK = 244;

bool key_pressed[KEY_COUNT];

int main(int argc, char* argv[])
{

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH | GLUT_RGB);
	glutInitWindowSize(680, 520);
	glutInitWindowPosition(0, 0);
	glutCreateWindow("Assignment 5");
	glutKeyboardFunc(keyboard);
	glutKeyboardUpFunc(keyboardUp);
	glutSpecialFunc(special);
	glutSpecialUpFunc(specialUp);
	glutIdleFunc(update);
	glutReshapeFunc(reshape);
	glutDisplayFunc(display);

	// glut initialization
	TimeSystem::init(5, 60, 0.1);
	init();
	// world initialization
	TimeSystem::markPauseEnd();

	glutMainLoop();

	return 1;
}

//set up data for use later in the program
void init()
{
	for(int i = 0; i < 258; i++){
		key_pressed[i] = false;
	}
  
	world.init();
	initDisplay();
}


void initDisplay()
{
	glClearColor(0.5, 0.5, 0.5, 0.0);
	glColor3f(0.0, 0.0, 0.0);
	glEnable(GL_DEPTH_TEST);
	glutPostRedisplay();
}


// this fucntion is called when a key is pressed
void keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27: // on [ESC]
		exit(0); // normal exit
		break;
	case ' ': // on space move forward
		//coordinateSystem.moveForward(); 
		key_pressed[KEY_SPACE] = true;
		break;
	case '/': 
		// move back
		key_pressed[KEY_BACK] = true;
		break;
	case 'H':
	case 'h':
		coordinateSystem.rotateCameraToVector(); // rotate camera
		break;
	case 'A':
	case 'a':
		 // move to left
		key_pressed[KEY_A] = true;
		break;
	case 'D':
	case 'd':
		// move right
		key_pressed[KEY_D] = true;
		break;
	case 'W':
	case 'w':
		// move up
		key_pressed[KEY_W] = true;
		break;
	case 'S':
	case 's':
		// move down
		key_pressed[KEY_S] = true;
		break;
	case 'F':
	case 'f':
		// move down
		key_pressed[KEY_F] = true;
		break;

	case 'T':
	case 't':
		coordinateSystem.setOrientation(Vector3::getRandomUnitVector());
	case 'U':
	case 'u':
		coordinateSystem.rotateUpright(0.1);
		break;

	case 'R':
	case 'r':
		break;

	}
}


void keyboardUp(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27: // on [ESC]
		exit(0); // normal exit
		break;
	case ' ': // on space move forward
		key_pressed[KEY_SPACE] = false;
		break;
	case '/': // move back
		key_pressed[KEY_BACK] =  false;
		break;
	case 'H':
	case 'h':
		coordinateSystem.rotateCameraToVector(); // rotate camera
		break;
	case 'A':
	case 'a': // move to left
		key_pressed[KEY_A] =  false;
		break;
	case 'D':
	case 'd': // move right
		key_pressed[KEY_D] =  false;
		break;
	case 'W':
	case 'w': // move up
		key_pressed[KEY_W] =  false;
		break;
	case 'S':
	case 's': // move down
		key_pressed[KEY_S] =  false;
		break;
	case 'F':
	case 'f': // move down
		key_pressed[KEY_F] =  false;
		break;
	}
}


// this function is called when an special key is pressed
void special(int special_key, int x, int y)
{
	switch(special_key)
	{
	case GLUT_KEY_RIGHT:
		// camera rotate right
		key_pressed[KEY_RIGHT_ARROW] =  true;
		break;
	case GLUT_KEY_LEFT:
		// camera rotate left
		key_pressed[KEY_LEFT_ARROW] =  true;
		break;
	case GLUT_KEY_UP:
		// camera rotate up
		key_pressed[KEY_UP_ARROW] =  true;
		break;
	case GLUT_KEY_DOWN:
		// camera rotate down
		key_pressed[KEY_DOWN_ARROW] =  true;
		break;
	}
}


void specialUp(int special_key, int x, int y)
{
	switch(special_key)
	{
	case GLUT_KEY_RIGHT:
		// camera rotate right
		key_pressed[KEY_RIGHT_ARROW] =  false;
		break;
	case GLUT_KEY_LEFT:
		// camera rotate left
		key_pressed[KEY_LEFT_ARROW] =  false;
		break;
	case GLUT_KEY_UP:
		// camera rotate up
		key_pressed[KEY_UP_ARROW] =  false;
		break;
	case GLUT_KEY_DOWN:
		// camera rotate down
		key_pressed[KEY_DOWN_ARROW] =  false;
		break;
	}
}


void update()
{


	if(key_pressed[KEY_RIGHT_ARROW]){	
		world.getPlayerShip().rotateAroundUp(0.05);// camera rotate right
	}
	if(key_pressed[KEY_LEFT_ARROW] ){
		world.getPlayerShip().rotateAroundUp(-0.05);
	}

	if(key_pressed[KEY_UP_ARROW]){
		world.getPlayerShip().rotateAroundRight(0.05);
	}

	if(	key_pressed[KEY_DOWN_ARROW] ){
		world.getPlayerShip().rotateAroundRight(-0.05);
	}
	if(key_pressed[KEY_A]){
		coordinateSystem.moveLeft(); // move to left
	}
	if(key_pressed[KEY_BACK]){
		coordinateSystem.moveBackwards(); // move back
	}
	if(key_pressed[KEY_SPACE]){
		world.playerShipFireBullet();
	}	
	if(key_pressed[KEY_S]){
		world.getPlayerShip().setSpeed(50);
	}
	else if(key_pressed[KEY_F]){
		world.getPlayerShip().setSpeed(2500);
	}
	else
		world.getPlayerShip().setSpeed(250);


	world.updateAll();

	TimeSystem::getTimeToNextFrame();
	TimeSystem::markFrameEnd();
	glutPostRedisplay();
}


void reshape(int w, int h)
{
	glViewport (0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60, (GLdouble)w / (GLdouble)h, 1.0, 1000000.0); 
	glMatrixMode(GL_MODELVIEW);
	glutPostRedisplay();
}


//this function does the rendering.
void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	// clear the screen - any drawing before here will not display
	glLoadIdentity();

	// set camera position
	world.getPlayerShip().setupCamera();

	//draw skybox
	world.drawSkybox();

	CoordinateSystem camera = world.getPlayerShip().getCameraCoordinateSystem();
	world.draw(camera.getForward(), camera.getUp());

	world.drawRingParticles();

	world.drawRings();
	// send the current image to the screen - any drawing after here will not display
	glutSwapBuffers();
}




























































































